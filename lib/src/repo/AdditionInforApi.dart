import 'package:HirohataCargo/src/models/infor.dart';
import 'package:HirohataCargo/src/utils/NetworkUtil.dart';
import 'package:dartz/dartz.dart';
import 'package:http/http.dart';

 class AdditionInforApi {
  Future<Either<Exception, Response>> getAdditionInfor(String order) async {
    try {
      final url = NetworkUtil.BASE_URL + '/api/getInfo?productCode=$order';
      Response response =
          await post(url, headers: NetworkUtil.getRequestHeaders()).timeout(
        Duration(seconds: 10),
      );
      return Right(response);
    } catch (e) {
      return Left(e);
    }
  }
}
