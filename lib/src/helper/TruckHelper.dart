import 'dart:convert';

import 'package:HirohataCargo/src/models/glitch/NoInternetGlitch.dart';
import 'package:HirohataCargo/src/models/glitch/glitch.dart';
import 'package:HirohataCargo/src/models/truck.dart';
import 'package:HirohataCargo/src/repo/TruckApi.dart';
import 'package:HirohataCargo/src/utils/constant.dart';
import 'package:dartz/dartz.dart';

class TruckHelper {
  final api = TruckApi();
  Future<Either<Glitch, Truck>> getTruck(String order) async {
    final apiResult = await api.getTruckType(order);
    return apiResult.fold((l) {
      return Left(NoInternetGlitch());
    }, (r) {
      if (r.statusCode == 200) {
        final data = jsonDecode(r.body);
        if (data != null && data.length > 0) {
          Truck truck;
          if (data.toString() == "1")
            truck = Constants.truck4t;
          else if (data.toString() == "2")
            truck = Constants.truck10t;
          else
            truck = Constants.truckContainer;
          return Right(truck);
        }
      }
      return Right(null);
    });
  }

  Future<Either<Glitch, bool>> updateTruckType(
      String order, String type) async {
    final apiResult = await api.updateTruckType(order, type);
    return apiResult.fold((l) {
      return Left(NoInternetGlitch());
    }, (r) {
      if (r.statusCode == 200) {
        return Right(true);
      }
      return Right(false);
    });
  }
}
