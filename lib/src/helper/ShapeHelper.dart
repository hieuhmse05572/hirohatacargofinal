import 'dart:convert';

import 'package:HirohataCargo/src/models/glitch/NoInternetGlitch.dart';
import 'package:HirohataCargo/src/models/glitch/glitch.dart';
import 'package:HirohataCargo/src/models/metal.dart';
import 'package:HirohataCargo/src/repo/ShapeApi.dart';
import 'package:HirohataCargo/src/utils/constant.dart';
import 'package:dartz/dartz.dart';

class ShapeHelper {
  final api = ShapeApi();
  Future<Either<Glitch, List<Metal>>> getShapes(String order) async {
    final apiResult = await api.getShapes(order);
    return apiResult.fold((l) {
      return Left(NoInternetGlitch());
    }, (r) {
      if (r.statusCode == 200) {
        final data = jsonDecode(r.body);
        if (data != null && data.length > 0) {
          List<Metal> metals = List();
          for (Map i in data) {
            Metal metal = Metal.fromJson(i);
            if (metal != null) {
              Metal clone = Metal.scaleTo(metal, Configs.scale);
              metals.add(clone);
            }
          }
          return Right(metals);
        }
      }
      return Right([]);
    });
  }

  Future<Either<Glitch, List<Metal>>> getShapesToEdit(String order) async {
    final apiResult = await api.getShapes(order);
    return apiResult.fold((l) {
      return Left(NoInternetGlitch());
    }, (r) {
      if (r.statusCode == 200) {
        final data = jsonDecode(r.body);
        if (data != null && data.length > 0) {
          List<Metal> metals = List();
          for (Map i in data) {
            Metal metal = Metal.fromJson(i);
            if (metal != null) {
              Metal clone = Metal.cloneToEdit(metal, Configs.scaleToEdit);
              metals.add(clone);
            }
          }
          return Right(metals);
        }
      }
      return Right([]);
    });
  }

  Future<Either<Glitch, List<Metal>>> getResetShape(String order) async {
    final apiResult = await api.getResetShape(order);
    return apiResult.fold((l) {
      return Left(NoInternetGlitch());
    }, (r) {
      if (r.statusCode == 200) {
        final data = jsonDecode(r.body);
        if (data != null && data.length > 0) {
          List<Metal> metals = List();
          for (Map i in data) {
            Metal metal = Metal.fromJson(i);
            if (metal != null) {
              Metal clone = Metal.cloneToEdit(metal, Configs.scaleToEdit);
              metals.add(clone);
            }
          }
          return Right(metals);
        }
      }
      return Right([]);
    });
  }
}
