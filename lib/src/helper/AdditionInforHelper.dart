import 'dart:convert';

import 'package:HirohataCargo/src/models/glitch/NoInternetGlitch.dart';
import 'package:HirohataCargo/src/models/glitch/glitch.dart';
import 'package:HirohataCargo/src/models/infor.dart';
import 'package:HirohataCargo/src/repo/AdditionInforApi.dart';
import 'package:dartz/dartz.dart';

class AdditionInforHelper {
  final api = AdditionInforApi();
  Future<Either<Glitch, Infor>> getAdditionInfor(String order) async {
    final apiResult = await api.getAdditionInfor(order);
    return apiResult.fold((l) {
      return Left(NoInternetGlitch());
    }, (r) {
      if (r.statusCode == 200) {
        final data = jsonDecode(r.body);
        if (data != null) {
          Infor infor = Infor.fromJson(data);
          return Right(infor);
        }
      }
      return Right(null);
    });
  }
}
