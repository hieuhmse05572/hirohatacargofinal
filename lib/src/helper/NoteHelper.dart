import 'dart:convert';

import 'package:HirohataCargo/src/models/glitch/NoInternetGlitch.dart';
import 'package:HirohataCargo/src/models/glitch/glitch.dart';
import 'package:HirohataCargo/src/models/note.dart';
import 'package:HirohataCargo/src/repo/NoteApi.dart';
import 'package:dartz/dartz.dart';

class NoteHelper {
  final api = NoteApi();
  Future<Either<Glitch, List<Note>>> getNotes() async {
    final apiResult = await api.getNotes();
    return apiResult.fold((l) {
      return Left(NoInternetGlitch());
    }, (r) {
      if (r.statusCode == 200) {
        final data = jsonDecode(r.body);
        if (data != null) {
          List<Note> notes = List();
          for (Map i in data) {
            Note o = Note.fromJson(i);
              notes.add(o);

          }
          return Right(notes);
        }
      }
      return Right([]);
    });
  }
}
