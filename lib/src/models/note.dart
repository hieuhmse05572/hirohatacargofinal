class Note{
  String id;
  String note;
  bool checked;

  Note(this.id, this.note, this.checked);
  factory Note.fromJson(Map<String, dynamic> json) => Note(
      json["id"],
      json["note"],
      false
  );
  factory Note.clone(Note note) => Note(
      note.id,
      note.note,
      note.checked
  );
}