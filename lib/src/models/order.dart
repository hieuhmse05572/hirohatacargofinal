class Order {

   final String productCode;
   final String flightCode;
   final String tmpVehicleNumber;
   final String scheduledShippingDate;
   final String stackingCode;

  const Order(this.productCode, this.flightCode, this.tmpVehicleNumber, this.scheduledShippingDate, this.stackingCode);

   factory Order.fromJson(Map<String, dynamic> json) => Order(
     json["productCode"],
     json["flightCode"],
     json["tmpVehicleNumber"],
     json["scheduledShippingDate"],
     json["stackingCode"] == null ? "" : json["stackingCode"]
   );
}
