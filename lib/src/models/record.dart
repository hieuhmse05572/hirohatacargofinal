class Record {
  int no;
  String product_code;
  String flight_code;
  String invoice_shipping_location;
  String invoice_transaction_type;
  String invoice_serial_number;
  String invoice_line_number;
  String tmp_vehicle_number;
  String real_vehicle_number;
  String shipping_building;
  double row_number;
  double column_number; // +
  double front_and_back;
  String material_name;
  String cross_sectional_dim;
  double length;
  double number_of_individuals;
  String specific_product;
  double height;
  double width;
  String scheduled_shipping_date;
  String anointingClassification; // co dau ko sau
  String contract_number;
  double widthOfRow = -1;
  int duplicateN = 0;
  String column_range = "";
  int id = -1;
  double weight;
  int sumOfMetal;
  bool isHighlight = false;
  List<int> ids = List();
  String stackingCode;

// 10 ma san pham
// 11 code chuyen bay
// 12 hoadon-dia diem ship
// 13 hoadon-loai giao dich
// 14 hoadon-so seri
// 15 hoadon-so hang
// 16 bien so xe tam thoi
// 18 so xe that
// 17 cong ship
// 20 tầng
// 21 cột
// 22 sắp xếp trước sau
// 28 loại thép
// 30 kích thước mặt cắt ngang
// 32 length (<=99999)
// 38 tổng số thanh trong khối kim loại
// 39 chủng loại sp
// 40 height
// 41 width
// 43 ngay chuyen hang du kien
// 48 hop dong No
  Record(
      int no,
      String product_code,
      String flight_code,
      String invoice_shipping_location,
      String invoice_transaction_type,
      String invoice_serial_number,
      String invoice_line_number,
      String tmp_vehicle_number,
      String real_vehicle_number,
      String shipping_building,
      double row_number,
      double column_number,
      double front_and_back,
      String material_name,
      String cross_sectional_dim,
      double length,
      double number_of_individuals,
      String specific_product,
      double height,
      double width,
      String scheduled_shipping_date,
      String anointingClassification,
      String contract_number,
      double weight,
      String stackingCode) {
    this.no = no;
    this.product_code = product_code;
    this.flight_code = flight_code;
    this.invoice_shipping_location = invoice_shipping_location;
    this.invoice_transaction_type = invoice_transaction_type;
    this.invoice_serial_number = invoice_serial_number;
    this.invoice_line_number = invoice_line_number;
    this.tmp_vehicle_number = tmp_vehicle_number;
    this.real_vehicle_number = real_vehicle_number;
    this.shipping_building = shipping_building;
    this.row_number = row_number;
    this.column_number = column_number;
    this.front_and_back = front_and_back;
    this.material_name = material_name;
    this.cross_sectional_dim = cross_sectional_dim;
    this.length = length;
    this.number_of_individuals = number_of_individuals;
    this.specific_product = specific_product;
    this.height = height;
    this.width = width;
    this.scheduled_shipping_date = scheduled_shipping_date;
    this.anointingClassification = anointingClassification;
    this.contract_number = contract_number;
    this.weight = weight;
    this.stackingCode = stackingCode;
  }

  factory Record.fromJson(Map<String, dynamic> json) => Record(
      json["no"] as int,
      json["product_code"],
      json["flight_code"],
      json["invoice_shipping_location"],
      json["invoice_transaction_type"],
      json["invoice_serial_number"],
      json["invoice_line_number"],
      json["tmp_vehicle_number"],
      json["real_vehicle_number"],
      json["shipping_building"],
      json["row_number"] as double,
      json["column_number"] as double,
      json["front_and_back"] as double,
      json["material_name"],
      json["cross_sectional_dim"],
      json["length"] as double,
      json["number_of_individuals"] as double,
      json["specific_product"],
      json["height"] as double,
      json["width"] as double,
      json["scheduled_shipping_date"],
      json["anointingClassification"],
      json["contract_number"],
      json["weight"] as double,
      json["stackingCode"] == null ? "" : json["stackingCode"]);
}
