import 'dart:ffi';
import 'dart:math';

import 'package:HirohataCargo/src/models/metal.dart';
import 'package:HirohataCargo/src/utils/colors.dart';
import 'package:HirohataCargo/ui/painter/renderText.dart';
import 'package:flutter/material.dart';

class BoxPainterOnDrag extends CustomPainter {
  BoxPainterOnDrag(this.rect);
  final Rect rect;

  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..strokeWidth = 0
      ..color = ColorsOf.border1
      ..style = PaintingStyle.stroke;
    // canvas.drawRect(rect, paint);
    var borderPain = Paint()
      ..strokeWidth = 5
      ..color = Colors.red
      ..style = PaintingStyle.stroke;
    var paint2 = Paint()
      ..style = PaintingStyle.fill
      ..color = Colors.grey.withOpacity(0.7);
    double border = 1;
    Rect inner = Rect.fromLTWH(rect.left + border, rect.top + border,
        rect.width - 2 * border, rect.height - 2 * border);
    Rect borderRect = Rect.fromLTWH(
        rect.left + border + 5,
        rect.top + border + 5,
        rect.width - 2 * (border + 5),
        rect.height - 2 * (border + 5));
    canvas.drawRect(rect, paint);
    canvas.drawRect(inner, paint2);
    canvas.drawRect(borderRect, borderPain);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

class BoxPainterOnTap extends CustomPainter {
  BoxPainterOnTap(this.rect);
  final Rect rect;

  @override
  void paint(Canvas canvas, Size size) {
    var borderPain = Paint()
      ..strokeWidth = 3
      ..color = Colors.red.withOpacity(0.6)
      ..style = PaintingStyle.stroke;
    var circle = Paint()
      ..style = PaintingStyle.fill
      ..color = Colors.red;
    canvas.drawCircle(rect.topLeft, 6, circle);
    canvas.drawCircle(rect.topRight, 6, circle);
    canvas.drawCircle(rect.bottomRight, 6, circle);
    canvas.drawCircle(rect.bottomLeft, 6, circle);
    canvas.drawLine(rect.topLeft, rect.topRight, borderPain);
    canvas.drawLine(rect.bottomLeft, rect.bottomRight, borderPain);
    canvas.drawLine(rect.topLeft, rect.bottomLeft, borderPain);
    canvas.drawLine(rect.topRight, rect.bottomRight, borderPain);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

class BoxPainter1 extends CustomPainter {
  BoxPainter1(this.metal, this.scale, this.canEdit, this.hasBothType);
  final Metal metal;
  final double scale;
  final bool canEdit;
  final bool hasBothType;
  double num = 1;
  double fontsize = 9;
  @override
  void paint(Canvas canvas, Size size) {
    if (canEdit) {
      num = 1 / 3.0;
    } else
      fontsize = 3.0 * scale;

    var paint = Paint()
      ..strokeWidth = 0
      ..color = ColorsOf.border1
      ..style = PaintingStyle.stroke;
    var borderPain = Paint()
      ..strokeWidth = 4
      ..color = ColorsOf.border1
      ..style = PaintingStyle.stroke;

    var paint2 = Paint()
      ..style = PaintingStyle.fill
      ..color = Colors.white;
    double border = 1;
    var paint1 = Paint()
      ..color = ColorsOf.border1After
      ..style = PaintingStyle.fill;
    var paint11 = Paint()
      ..strokeWidth = 1
      ..color = Colors.white
      ..style = PaintingStyle.stroke;

    // render After
    Rect rect1 = Rect.fromLTWH(metal.left * scale, metal.top * scale,
        metal.width * scale, metal.height * scale);
    Rect inner1 = Rect.fromLTWH(rect1.left + border, rect1.top + border,
        rect1.width - 2 * border, rect1.height - 2 * border);

    canvas.drawRect(rect1, paint1);
    canvas.drawRect(inner1, paint11);

    // render Before

    Rect rect = Rect.fromLTWH(
        metal.left * scale,
        (metal.top + metal.heights  ) * scale,
        (metal.width - metal.widths ) * scale,
        (metal.height - metal.heights ) * scale);
    Rect inner = Rect.fromLTWH(rect.left + border, rect.top + border,
        rect.width - 2 * border, rect.height - 2 * border);
    Rect borderRect = Rect.fromLTWH(
        rect.left + border + 5,
        rect.top + border + 5,
        rect.width - 2 * (border + 5),
        rect.height - 2 * (border + 5));

    canvas.drawRect(rect, paint);
    canvas.drawRect(inner, paint2);
    canvas.drawRect(borderRect, borderPain);

    if (metal.width >= 15.0) {
      if (metal.widths > 0) {
        fontsize = 8;
        double front = (metal.width * 20 * num) / 1000;
        Size sizeOfText = getSizeOfText(
            "${front}/${(front)}",
            TextStyle(fontSize: fontsize));
        canvas.drawRect(
            Rect.fromLTWH(rect.left + (rect.width - sizeOfText.width) / 2,
                rect.top + 2, sizeOfText.width, sizeOfText.height),
            paint2);

        TextUtil.drawTextWidth(
            canvas,
            metal.width * 20 * num,
            (metal.width - metal.widths) * 20 * num,
            rect.left + (rect.width - sizeOfText.width) / 2,
            rect.top + 2,
            fontsize);
      } else {
        fontsize = 10;
        double width = double.parse(((metal.width * 20 * num) / 1000).toStringAsFixed(3));
        Size sizeOfText = getSizeOfText("${width}",
            TextStyle(fontSize: fontsize));
        canvas.drawRect(
            Rect.fromLTWH(rect.left + (rect.width - sizeOfText.width) / 2,
                rect.top + 2, sizeOfText.width, sizeOfText.height),
            paint2);

        TextUtil.drawText(
            canvas,
            metal.width * 20 * num,
            rect.left + (rect.width - sizeOfText.width) / 2,
            rect.top + 2,
            fontsize);
      }
      if (metal.heights > 0) {
        fontsize = 8;
        double front =(metal.height * 20 * num) / 1000;
        Size sizeOfText =
        getSizeOfText("${front}", TextStyle(fontSize: fontsize));
        canvas.drawRect(
            Rect.fromLTWH(
                rect.left + 2,
                rect.top + (rect.height - sizeOfText.height * 2) / 2,
                sizeOfText.width,
                sizeOfText.height * 2),
            paint2);
        TextUtil.drawTextHeight(
            canvas,
            metal.height * 20 * num,
            (metal.height - metal.heights) * 20 * num,
            rect.left + 2,
            rect.top + (rect.height - sizeOfText.height * 2) / 2,
            fontsize);
      } else {
        fontsize = 10;
        Size sizeOfText = getSizeOfText("${(metal.height * 20 * num) / 1000}",
            TextStyle(fontSize: fontsize));
        canvas.drawRect(
            Rect.fromLTWH(
                rect.left + 2,
                rect.top + (rect.height - sizeOfText.height) / 2,
                sizeOfText.width,
                sizeOfText.height),
            paint2);
        TextUtil.drawText(canvas, metal.height * 20 * num, rect.left + 2,
            rect.top + (rect.height - sizeOfText.height) / 2, fontsize);
      }
      if (metal.heights > 0 ||metal.widths >0 || metal.lengths >0 ) {
        Size sizeOfText = getSizeOfText(
            "${((metal.length + metal.lengths) * 20 * num) / 1000}",
            TextStyle(
              color: Colors.brown,
              fontSize: fontsize,
              fontWeight: FontWeight.bold,
            ));
        TextUtil.drawLengthOfMetal(
            canvas,
            (metal.length + metal.lengths) * 20 * num,
            metal.length * 20 * num,
            rect.center.dx - sizeOfText.width / 2,
            rect.center.dy + sizeOfText.height * 1 / 3,
            fontsize);
      }else{
        double leng = double.parse((((metal.length) * 20 * num) / 1000).toStringAsFixed(3));
        Size sizeOfText = getSizeOfText(
            "${leng}",
            TextStyle(
              color: Colors.brown,
              fontSize: fontsize,
              fontWeight: FontWeight.bold,
            ));
        TextUtil.drawTextCenter(
            canvas,
            metal.length * 20 * num,
            rect.center.dx - sizeOfText.width / 2,
            rect.center.dy + sizeOfText.height * 1 / 3,
            fontsize);
      }
    }
  }

  Size getSizeOfText(String text, TextStyle style) {
    final TextPainter textPainter = TextPainter(
        text: TextSpan(text: text, style: style),
        maxLines: 1,
        textDirection: TextDirection.ltr)
      ..layout(minWidth: 0, maxWidth: double.infinity);
    return textPainter.size;
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

class InforBox1 extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..strokeWidth = 0
      ..color = ColorsOf.border1
      ..style = PaintingStyle.stroke;
    // canvas.drawRect(rect, paint);
    var borderPain = Paint()
      ..strokeWidth = 5
      ..color = ColorsOf.border1
      ..style = PaintingStyle.stroke;

    var paint2 = Paint()
      ..style = PaintingStyle.fill
      ..color = Colors.white;
    double border = 1;
    Rect rect = Rect.fromLTWH(10, 50, 50, 30);
    Rect inner = Rect.fromLTWH(rect.left + border, rect.top + border,
        rect.width - 2 * border, rect.height - 2 * border);
    Rect borderRect = Rect.fromLTWH(
        rect.left + border + 5,
        rect.top + border + 5,
        rect.width - 2 * (border + 5),
        rect.height - 2 * (border + 5));

    canvas.drawRect(rect, paint);
    canvas.drawRect(inner, paint2);
    canvas.drawRect(borderRect, borderPain);
    TextUtil.drawTextInfor(canvas, "  油有", rect.right, rect.top + 5);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

class InforBox2 extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..strokeWidth = 0
      ..color = ColorsOf.border2
      ..style = PaintingStyle.stroke;
    // canvas.drawRect(rect, paint);
    var borderPain = Paint()
      ..strokeWidth = 5
      ..color = ColorsOf.border2
      ..style = PaintingStyle.stroke;

    var paint2 = Paint()
      ..style = PaintingStyle.fill
      ..color = Colors.white;
    double border = 1;
    Rect rect = Rect.fromLTWH(10, 10, 50, 30);
    Rect inner = Rect.fromLTWH(rect.left + border, rect.top + border,
        rect.width - 2 * border, rect.height - 2 * border);
    Rect borderRect = Rect.fromLTWH(
        rect.left + border + 5,
        rect.top + border + 5,
        rect.width - 2 * (border + 5),
        rect.height - 2 * (border + 5));

    canvas.drawRect(rect, paint);
    canvas.drawRect(inner, paint2);
    canvas.drawRect(borderRect, borderPain);

    TextUtil.drawTextInfor(canvas, "  塗装", rect.right, rect.top + 5);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

class InforBox3 extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..strokeWidth = 1
      ..color = Colors.brown
      ..style = PaintingStyle.fill;
    // canvas.drawRect(rect, paint);

    var lineInner = Paint()
      ..strokeWidth = 2
      ..color = Colors.brown
      ..style = PaintingStyle.stroke;

    double border = 1;
    Rect rect = Rect.fromLTWH(10, 90, 50, 30);
    // Rect inner = Rect.fromLTWH(rect.left + border, rect.top + border,
    //     rect.width - 2 * border, rect.height - 2 * border);
    // Rect borderRect = Rect.fromLTWH(
    //     rect.left + border + 5,
    //     rect.top + border + 5,
    //     rect.width - 2 * (border + 5),
    //     rect.height - 2 * (border + 5));

    canvas.drawRect(rect, paint);
    // double count = 0;
    // while (count < rect.width - 15) {
    //   Path path = Path();
    //   path.moveTo(rect.left + count, rect.bottom);
    //   path.lineTo(rect.left + 15 + count, rect.top);
    //   canvas.drawPath(path, lineInner);
    //   count += 10;
    // }
    // canvas.drawRect(inner, paint2);
    // canvas.drawRect(borderRect, borderPain);

    // TextUtil.drawTextCenter(
    //     canvas,
    //    1,
    //     rect.center.dx ,
    //     rect.center.dy);
    TextUtil.drawTextInfor(canvas, "じん木", rect.right + 3, rect.top + 5);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

class InforColor extends CustomPainter {
  InforColor(this.truckName, this.length);
  final String truckName;
  final int length;
  @override
  void paint(Canvas canvas, Size size) {
    // canvas.drawRect(rect, paint);

    var linePatin = Paint()
      ..strokeWidth = 3
      ..color = Colors.brown
      ..style = PaintingStyle.fill;
    // Rect rect2 = Rect.fromLTWH(40, 140,
    //     15, 15);
    double count = 1;
    TextUtil.drawTextInfor(canvas, "行番号: ", 10, 140);
    for (int i = 1; i < length + 1; i++) {
      var paint = Paint()
        ..strokeWidth = 1
        ..color = ColorsOf.colors[i]
        ..style = PaintingStyle.fill;
      Rect rect = Rect.fromLTWH(i * 20.0, 180, 15, 15);
      TextUtil.drawTextInfor(
          canvas, "${i.toInt()}", rect.center.dx - 3, rect.center.dy - 25);
      canvas.drawRect(rect, paint);
    }

    // TextUtil.drawTextInfor(canvas, "Unit: m", 10, 210);

    TextUtil.drawTextInfor(canvas, "車 種:", 10, 210);
    TextUtil.drawTextInfor(canvas, "${truckName}", 20, 230);

    // canvas.drawLine(Offset(10, 185), Offset(20 * count + 10,  185), linePatin);
    // canvas.drawLine(Offset(20 * count +5, 180), Offset(20 * count + 10,  185), linePatin);
    // canvas.drawLine(Offset(20 * count +5, 190), Offset(20 * count + 10,  185), linePatin);

    // canvas.drawRect(rect, paint);
    // canvas.drawRect(rect2, paint);

    // TextUtil.drawTextInfor(
    //     canvas,
    //     " Gỗ kê",
    //     rect.right,
    //     rect.center.dy);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

class BoxPainter2 extends CustomPainter {
  BoxPainter2(this.metal, this.scale, this.canEdit, this.hasBothType);
  final double scale;
  final Metal metal;
  bool canEdit;
  bool hasBothType;
  double num = 1;
  double fontsize = 9;
  @override
  void paint(Canvas canvas, Size size) {
    if (canEdit)
      num = 1 / 3.0;
    else
      fontsize = 3.0 * scale;
    var paint = Paint()
      ..strokeWidth = 0
      ..color = ColorsOf.border2
      ..style = PaintingStyle.stroke;
    var lineInner = Paint()
      ..strokeWidth = 0.5
      ..color = Colors.grey
      ..style = PaintingStyle.stroke;
    var paint2 = Paint()
      ..style = PaintingStyle.fill
      ..color = Colors.white;
    var borderPain = Paint()
      ..strokeWidth = 4
      ..color = ColorsOf.border2
      ..style = PaintingStyle.stroke;
    // var borderNilon = Paint()
    //   ..strokeWidth = 2
    //   ..color = Colors.pink
    //   ..style = PaintingStyle.stroke;
    double border = 1;

    var paint1 = Paint()
      ..color = ColorsOf.border1After
      ..style = PaintingStyle.fill;
    var paint11 = Paint()
      ..strokeWidth = 1
      ..color = Colors.white
      ..style = PaintingStyle.stroke;

    // render After
    Rect rect1 = Rect.fromLTWH(metal.left * scale, (metal.top) * scale,
        (metal.width) * scale, (metal.height) * scale);
    Rect inner1 = Rect.fromLTWH(rect1.left + border, rect1.top + border,
        rect1.width - 2 * border, rect1.height - 2 * border);

    canvas.drawRect(rect1, paint1);
    canvas.drawRect(inner1, paint11);

    // render Before
    Rect rect = Rect.fromLTWH(
        metal.left * scale,
        (metal.top + metal.heights  ) * scale,
        (metal.width - metal.widths ) * scale,
        (metal.height - metal.heights ) * scale);

    Rect inner = Rect.fromLTWH(rect.left + border, rect.top + border,
        rect.width - 2 * border, rect.height - 2 * border);
    Rect borderRect = Rect.fromLTWH(
        rect.left + border + 5,
        rect.top + border + 5,
        rect.width - 2 * (border + 5),
        rect.height - 2 * (border + 5));
    // Rect RectNilon = Rect.fromLTWH(
    //     rect.left + border -1 ,
    //     rect.top + border -1,
    //     rect.width - 2 * (border-1 ),
    //     rect.height - 2 * (border-1 ));
    canvas.drawRect(rect, paint);
    canvas.drawRect(inner, paint2);
    canvas.drawRect(borderRect, borderPain);
    // if(hasBothType == true)
    //   canvas.drawRect(RectNilon, borderNilon);
    // double count = 7;
    // while (count < rect.height - 10) {
    //   Path path = Path();
    //   path.moveTo(rect.left + 7, rect.top + count);
    //   path.lineTo(rect.right - 7, rect.top + count);
    //   canvas.drawPath(path, lineInner);
    //   count += 3;
    // }
    // count = 7;
    // while (count < rect.width - 10) {
    //   Path path = Path();
    //   path.moveTo(rect.left + count, rect.bottom - 10);
    //   path.lineTo(rect.left + count, rect.top + 10);
    //   canvas.drawPath(path, lineInner);
    //   count += 3;
    // }
    if (metal.width >= 15.0) {
      if (metal.widths > 0) {
        fontsize = 8;
        double front = (metal.width * 20 * num) / 1000;
        Size sizeOfText = getSizeOfText(
            "${front}/${(front)}",
            TextStyle(fontSize: fontsize));
        canvas.drawRect(
            Rect.fromLTWH(rect.left + (rect.width - sizeOfText.width) / 2,
                rect.top + 2, sizeOfText.width, sizeOfText.height),
            paint2);

        TextUtil.drawTextWidth(
            canvas,
            metal.width * 20 * num,
            (metal.width - metal.widths) * 20 * num,
            rect.left + (rect.width - sizeOfText.width) / 2,
            rect.top + 2,
            fontsize);
      } else {
        fontsize = 10;
        Size sizeOfText = getSizeOfText("${(metal.width * 20 * num) / 1000}",
            TextStyle(fontSize: fontsize));
        canvas.drawRect(
            Rect.fromLTWH(rect.left + (rect.width - sizeOfText.width) / 2,
                rect.top + 2, sizeOfText.width, sizeOfText.height),
            paint2);

        TextUtil.drawText(
            canvas,
            metal.width * 20 * num,
            rect.left + (rect.width - sizeOfText.width) / 2,
            rect.top + 2,
            fontsize);
      }
      if (metal.heights > 0) {
        fontsize = 8;
        double front =(metal.height * 20 * num) / 1000;
        Size sizeOfText =
        getSizeOfText("${front}", TextStyle(fontSize: fontsize));
        canvas.drawRect(
            Rect.fromLTWH(
                rect.left + 2,
                rect.top + (rect.height - sizeOfText.height * 2) / 2,
                sizeOfText.width,
                sizeOfText.height * 2),
            paint2);
        TextUtil.drawTextHeight(
            canvas,
            metal.height * 20 * num,
            (metal.height - metal.heights) * 20 * num,
            rect.left + 2,
            rect.top + (rect.height - sizeOfText.height * 2) / 2,
            fontsize);
      } else {
        fontsize = 10;
        Size sizeOfText = getSizeOfText("${(metal.height * 20 * num) / 1000}",
            TextStyle(fontSize: fontsize));
        canvas.drawRect(
            Rect.fromLTWH(
                rect.left + 2,
                rect.top + (rect.height - sizeOfText.height) / 2,
                sizeOfText.width,
                sizeOfText.height),
            paint2);
        TextUtil.drawText(canvas, metal.height * 20 * num, rect.left + 2,
            rect.top + (rect.height - sizeOfText.height) / 2, fontsize);
      }
      if (metal.heights > 0 ||metal.widths >0 || metal.lengths >0 ) {
        Size sizeOfText = getSizeOfText(
            "${((metal.length + metal.lengths) * 20 * num) / 1000}",
            TextStyle(
              color: Colors.brown,
              fontSize: fontsize,
              fontWeight: FontWeight.bold,
            ));
        TextUtil.drawLengthOfMetal(
            canvas,
            (metal.length + metal.lengths) * 20 * num,
            metal.length * 20 * num,
            rect.center.dx - sizeOfText.width / 2,
            rect.center.dy + sizeOfText.height * 1 / 3,
            fontsize);
      }else{
        double leng = double.parse((((metal.length) * 20 * num) / 1000).toStringAsFixed(3));

        Size sizeOfText = getSizeOfText(
            "${leng}",
            TextStyle(
              color: Colors.brown,
              fontSize: fontsize,
              fontWeight: FontWeight.bold,
            ));
        TextUtil.drawTextCenter(
            canvas,
            metal.length * 20 * num,
            rect.center.dx - sizeOfText.width / 2,
            rect.center.dy + sizeOfText.height * 1 / 3,
            fontsize);
      }
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;

  Size getSizeOfText(String text, TextStyle style) {
    final TextPainter textPainter = TextPainter(
        text: TextSpan(text: text, style: style),
        maxLines: 1,
        textDirection: TextDirection.ltr)
      ..layout(minWidth: 0, maxWidth: double.infinity);
    return textPainter.size;
  }
}

class ShimPainter extends CustomPainter {
  final List<Point> points;
  ShimPainter(this.points, this.scale);
  final double scale;
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = ColorsOf.shim
      ..strokeWidth = 2
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;
    Path path = new Path();
    path.moveTo(points[0].x * scale, points[0].y * scale);
    // path.lineTo(posX + 40, posY);
    points.forEach((element) {
      path.lineTo(element.x * scale, element.y * scale);
    });
    path.close();
    var lineInner = Paint()
      ..strokeWidth = 2
      ..color = Colors.brown
      ..style = PaintingStyle.stroke;
    // path.lineTo(Config.bottomRight.x, Config.bottomRight.y);
    double minX = points[0].x;
    double minY = points[0].y;
    double maxX = points[0].x;
    double maxY = points[0].y;
    points.forEach((element) {
      if (element.x < minX) minX = element.x;
      if (element.y < minY) minY = element.y;
      if (element.x > maxX) maxX = element.x;
      if (element.y > maxY) maxY = element.y;
    });
    Offset topLeft = Offset(minX * scale, scale * minY);
    Offset bottomRight = Offset(maxX * scale, maxY * scale);
    Rect rect = Rect.fromPoints(topLeft, bottomRight);
    canvas.drawPath(path, paint);

    // double count = 0;
    // while(count< rect.height ){
    //   Path path = Path();
    //   path.moveTo(rect.left , rect.top+ count);
    //   path.lineTo(rect.right , rect.top+ count);
    //   canvas.drawPath(path,lineInner );
    //   count+= 3;
    // }
    double count = 0;
    while (count < rect.width - 15) {
      Path path = Path();
      path.moveTo(rect.left + count, rect.bottom);
      path.lineTo(rect.left + 15 + count, rect.top);
      canvas.drawPath(path, lineInner);
      count += 10;
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class BoxPainterTopView extends CustomPainter {
  BoxPainterTopView(this.metal, this.scale, this.min, this.padding,
      this.canEdit, this.color, this.posX);
  final Metal metal;
  final Point min;
  final double scale;
  final double padding;
  final bool canEdit;
  final Color color;
  final double posX;
  double num = 1;
  @override
  void paint(Canvas canvas, Size size) {
    if (canEdit) num = 1 / 3.0;

    // canvas.drawRect(rect, paint);
    var paint2 = Paint()
      ..style = PaintingStyle.fill
      ..color = Colors.white;
    var borderStyle = Paint()
      ..strokeWidth = 2
      ..style = PaintingStyle.stroke
      ..color = Colors.white;
    // ..color = metal.type == 1 ? ColorsOf.border1 : ColorsOf.border2;
    var fill = Paint()
      ..strokeWidth = 2
      ..color = color
      ..style = PaintingStyle.fill;

    double length = (metal.length + metal.lengths) * scale / 3;
    double length1 = (metal.length) * scale / 3;
    double top = length + 5;
    if(metal.heights == 0 && metal.widths == 0 && metal.lengths== 0)
      top = 0;
    Rect rect = Rect.fromLTWH(metal.left * scale / 2, metal.top * scale,
        (metal.width - metal.widths )* scale / 2, length1);
    Rect inner = Rect.fromLTWH(posX * scale - padding * scale + rect.left,
        115 * scale + top, rect.width, length1);
    Rect border = Rect.fromLTWH(posX * scale - padding * scale + rect.left,
        115 * scale + top, rect.width, length1);
    canvas.drawRect(inner, fill);
    canvas.drawRect(border, borderStyle);

    if (metal.width >= 13.0) {
      Size sizeOfText = getSizeOfText(
          "${(metal.length * 20 * num).round()}",
          TextStyle(
            color: Colors.brown,
            fontSize: 4 * scale,
            fontWeight: FontWeight.bold,
          ));

      canvas.drawRect(
          Rect.fromLTWH(
              inner.center.dx - sizeOfText.width / 2 + 2 * scale,
              inner.center.dy - sizeOfText.height / 2,
              sizeOfText.width - 4 * scale,
              sizeOfText.height),
          paint2);
      TextUtil.drawTextCenter(
          canvas,
          metal.length * 20 * num,
          inner.center.dx - sizeOfText.width / 2 + 3 * scale,
          inner.center.dy - sizeOfText.height / 2,
          3 * scale);
    }
    var rulerPain = Paint()
      ..strokeWidth = 1
      ..color = Colors.purpleAccent
      ..style = PaintingStyle.stroke;
    var circle = Paint()
      ..style = PaintingStyle.fill
      ..color = Colors.purpleAccent;

    Path path2 = new Path();
    if (metal.length == min.x || metal.length == min.y) {
      double posX = metal.length == min.x ? this.posX - 35.0 : this.posX - 50.0;
      path2.moveTo(inner.left - 3 * scale, inner.top);
      path2.lineTo(posX * scale, inner.top);

      path2.moveTo(posX * scale, inner.top);

      path2.lineTo(posX * scale, inner.bottom);

      canvas.drawCircle(Offset(posX * scale, inner.top), 1.5 * scale, circle);
      canvas.drawCircle(
          Offset(posX * scale, inner.bottom), 1.5 * scale, circle);

      path2.moveTo(inner.left - 3 * scale, inner.bottom);
      path2.lineTo(posX * scale, inner.bottom);

      canvas.drawPath(path2, rulerPain);
      TextUtil.drawTextWitFontSize(canvas, metal.length * 20 * num, 11,
          posX * scale - 7 * scale, inner.top + inner.height / 2);
    }
  }

  Size getSizeOfText(String text, TextStyle style) {
    final TextPainter textPainter = TextPainter(
        text: TextSpan(text: text, style: style),
        maxLines: 1,
        textDirection: TextDirection.ltr)
      ..layout(minWidth: 0, maxWidth: double.infinity);
    return textPainter.size;
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}



class BoxPainterTopViewBefore extends CustomPainter {
  BoxPainterTopViewBefore(this.metal, this.scale, this.min, this.padding,
      this.canEdit, this.color, this.posX);
  final Metal metal;
  final Point min;
  final double scale;
  final double padding;
  final bool canEdit;
  final Color color;
  final double posX;
  double num = 1;
  @override
  void paint(Canvas canvas, Size size) {
    if (canEdit) num = 1 / 3.0;

    // canvas.drawRect(rect, paint);
    var paint2 = Paint()
      ..style = PaintingStyle.fill
      ..color = Colors.white;
    var borderStyle = Paint()
      ..strokeWidth = 2
      ..style = PaintingStyle.stroke
      ..color = Colors.white;
    // ..color = metal.type == 1 ? ColorsOf.border1 : ColorsOf.border2;
    var fill = Paint()
      ..strokeWidth = 2
      ..color = color
      ..style = PaintingStyle.fill;

    double length = (metal.length + metal.lengths) * scale / 3;

    Rect rect = Rect.fromLTWH(metal.left * scale / 2, metal.top * scale ,
        metal.width * scale / 2, metal.height * scale);
    Rect inner = Rect.fromLTWH(posX * scale - padding * scale + rect.left,
        115 * scale , rect.width, length);
    Rect border = Rect.fromLTWH(posX * scale - padding * scale + rect.left,
        115 * scale , rect.width, length);

    canvas.drawRect(inner, fill);
    canvas.drawRect(border, borderStyle);

    if (metal.width >= 13.0) {
      Size sizeOfText = getSizeOfText(
          "${((metal.length + metal.lengths) * 20 * num).round()}",
          TextStyle(
            color: Colors.brown,
            fontSize: 4 * scale,
            fontWeight: FontWeight.bold,
          ));

      canvas.drawRect(
          Rect.fromLTWH(
              inner.center.dx - sizeOfText.width / 2 + 2 * scale,
              inner.center.dy - sizeOfText.height / 2,
              sizeOfText.width - 4 * scale,
              sizeOfText.height),
          paint2);
      TextUtil.drawTextCenter(
          canvas,
          (metal.length + metal.lengths) * 20 * num,
          inner.center.dx - sizeOfText.width / 2 + 3 * scale,
          inner.center.dy - sizeOfText.height / 2,
          3 * scale);
    }
    var rulerPain = Paint()
      ..strokeWidth = 1
      ..color = Colors.purpleAccent
      ..style = PaintingStyle.stroke;
    var circle = Paint()
      ..style = PaintingStyle.fill
      ..color = Colors.purpleAccent;

    Path path2 = new Path();
    if (metal.length == min.x || metal.length == min.y) {
      double posX = metal.length == min.x ? this.posX - 35.0 : this.posX - 50.0;
      path2.moveTo(inner.left - 3 * scale, inner.top);
      path2.lineTo(posX * scale, inner.top);

      path2.moveTo(posX * scale, inner.top);

      path2.lineTo(posX * scale, inner.bottom);

      canvas.drawCircle(Offset(posX * scale, inner.top), 1.5 * scale, circle);
      canvas.drawCircle(
          Offset(posX * scale, inner.bottom), 1.5 * scale, circle);

      path2.moveTo(inner.left - 3 * scale, inner.bottom);
      path2.lineTo(posX * scale, inner.bottom);

      canvas.drawPath(path2, rulerPain);
      TextUtil.drawTextWitFontSize(canvas,( metal.length + metal.lengths)* 20 * num, 11,
          posX * scale - 7 * scale, inner.top + inner.height / 2);
    }
  }

  Size getSizeOfText(String text, TextStyle style) {
    final TextPainter textPainter = TextPainter(
        text: TextSpan(text: text, style: style),
        maxLines: 1,
        textDirection: TextDirection.ltr)
      ..layout(minWidth: 0, maxWidth: double.infinity);
    return textPainter.size;
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

class BoxShimPainter extends CustomPainter {
  BoxShimPainter(this.topLeft, this.bottomRight);
  final Offset topLeft, bottomRight;
  @override
  void paint(Canvas canvas, Size size) {
    var paint2 = Paint()
      ..style = PaintingStyle.fill
      ..color = Colors.brown;
    var lineInner = Paint()
      ..strokeWidth = 2
      ..color = ColorsOf.shim
      ..style = PaintingStyle.stroke;
    Rect rect = Rect.fromPoints(topLeft, bottomRight);
    canvas.drawRect(rect, paint2);
    // double count = 0;
    // while (count < rect.width - 15) {
    //   Path path = Path();
    //   path.moveTo(rect.left + count, rect.bottom);
    //   path.lineTo(rect.left  + count, rect.top);
    //   canvas.drawPath(path, lineInner);
    //   count += 10;
    // }
    // while (count < rect.height - 10) {
    //   Path path = Path();
    //   path.moveTo(rect.left , rect.top + count);
    //   path.lineTo(rect.right , rect.top + count);
    //   canvas.drawPath(path, lineInner);
    //   count += 3;
    // }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
