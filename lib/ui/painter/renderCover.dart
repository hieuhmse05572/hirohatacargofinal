import 'dart:math';

import 'package:HirohataCargo/src/models/metal.dart';
import 'package:flutter/material.dart';

class CoverPainter extends CustomPainter {
  final Metal metal;
  int position = 1;
  double scale;
  CoverPainter({this.metal, this.position, this.scale});
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Colors.amber
      ..strokeWidth = 5
      ..strokeCap = StrokeCap.butt
      ..style = PaintingStyle.stroke;
    Path path = new Path();
    // 1 top
    // 2 right
    // 3 bottom
    // 4 left
    if (position == 3) {
      path.moveTo(metal.left * scale, metal.bottom * scale);
      path.lineTo(metal.right * scale, metal.bottom * scale);
      canvas.drawPath(path, paint);
    } else if (position == 1) {
      path.moveTo(metal.left * scale, metal.top * scale);
      path.lineTo(metal.right * scale, metal.top * scale);
      canvas.drawPath(path, paint);
    } else if (position == 2) {
      path.moveTo(metal.right * scale, metal.top * scale + 2 * scale);
      path.lineTo(metal.right * scale, metal.bottom * scale - 2 * scale);
      canvas.drawPath(path, paint);
    } else {
      path.moveTo(metal.left * scale, metal.top * scale + 2 * scale);
      path.lineTo(metal.left * scale, metal.bottom * scale - 2 * scale);

      canvas.drawPath(path, paint);
    }

    // canvas.drawLine(startingPoint, midPoint, paint);
    // canvas.drawLine(midPoint, endingPoint, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class CoverNilonPainter extends CustomPainter {
  final Metal metal;
  int position = 1;
  double scale;
  CoverNilonPainter({this.metal, this.position, this.scale});
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Colors.green
      ..strokeWidth = 2.5
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;
    Path path = new Path();
    // 1 top
    // 2 right
    // 3 bottom
    // 4 left
    if (position == 3) {
      path.moveTo(metal.left * scale , metal.bottom * scale);
      path.lineTo(metal.right * scale + 1, metal.bottom * scale);
      canvas.drawPath(path, paint);
    } else if (position == 1) {
      path.moveTo(metal.left * scale, metal.top * scale);
      path.lineTo(metal.right * scale + 1, metal.top * scale);
      canvas.drawPath(path, paint);
    } else if (position == 2) {
      path.moveTo(metal.right * scale, metal.top * scale );
      path.lineTo(metal.right * scale, metal.bottom * scale + 1 * scale);
      canvas.drawPath(path, paint);
    } else {
      path.moveTo(metal.left * scale, metal.top * scale);
      path.lineTo(metal.left * scale, metal.bottom * scale + 1 * scale);

      canvas.drawPath(path, paint);
    }

    // canvas.drawLine(startingPoint, midPoint, paint);
    // canvas.drawLine(midPoint, endingPoint, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class CoverNilonPainter1 extends CustomPainter {
  final Metal metal;
  int position = 1;
  double scale;
  double start;
  double end;
  CoverNilonPainter1(
      {this.metal, this.position, this.scale, this.start, this.end});
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Colors.green
      ..strokeWidth = 2.5
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;
    Path path = new Path();
    // 1 top
    // 2 right
    // 3 bottom
    // 4 left
    if (position == 3) {
      path.moveTo(start * scale, metal.bottom * scale);
      path.lineTo(end * scale + 0.5, metal.bottom * scale);
      canvas.drawPath(path, paint);
    } else if (position == 1) {
      path.moveTo(start * scale, metal.top * scale);
      path.lineTo(end * scale + 0.5, metal.top * scale);
      canvas.drawPath(path, paint);
    } else if (position == 2) {
      path.moveTo(metal.right * scale, metal.top * scale);
      path.lineTo(metal.right * scale, metal.bottom * scale + 0.5 * scale);
      canvas.drawPath(path, paint);
    } else {
      path.moveTo(metal.left * scale, metal.top * scale);
      path.lineTo(metal.left * scale, metal.bottom * scale + 0.5 * scale);

      canvas.drawPath(path, paint);
    }

    // canvas.drawLine(startingPoint, midPoint, paint);
    // canvas.drawLine(midPoint, endingPoint, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
