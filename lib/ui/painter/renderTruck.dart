import 'dart:math';

import 'package:HirohataCargo/src/models/truck.dart';
import 'package:HirohataCargo/src/utils/colors.dart';
import 'package:HirohataCargo/ui/painter/renderText.dart';
import 'package:flutter/material.dart';

class TruckPainter extends CustomPainter {
  TruckPainter(this.truck, this.scale, this.pos, this.width);
  final Truck truck;
  final Point pos;
  final double width;
  final double scale;
  @override
  void paint(Canvas canvas, Size size) {
    var borderTruck = Paint()
      ..strokeWidth = 2
      ..color = Colors.black
      ..style = PaintingStyle.stroke;
    var paintTruck = Paint()
      ..color = Colors.black.withOpacity(0.2)
      ..style = PaintingStyle.fill;
    // canvas.drawRect(rect, paint);
    var wheel = Paint()
      ..color = Colors.black
      ..style = PaintingStyle.fill;
    var rulerPain = Paint()
      ..strokeWidth = 1
      ..color = Colors.purpleAccent
      ..style = PaintingStyle.stroke;
    var circle = Paint()
      ..style = PaintingStyle.fill
      ..color = Colors.purpleAccent;
    Rect inner = Rect.fromLTWH(pos.x * scale, pos.y * scale,
        truck.width * scale / 2, truck.length * scale / 3);
    canvas.drawRect(inner, paintTruck);

    Rect rect11 = Rect.fromLTWH(
      inner.left - 2 * scale,
      inner.top - 10 * scale,
      inner.width + 4 * scale,
      scale * 8,
    );
    Rect rectLeft = Rect.fromLTWH(
      inner.left - 2 * scale,
      inner.top - 50 * scale,
      5 * scale,
      scale * 40,
    );
    Rect rectRight = Rect.fromLTWH(
      inner.right - 3 * scale,
      inner.top - 50 * scale,
      5 * scale,
      scale * 40,
    );

    canvas.drawRect(rect11, borderTruck);
    canvas.drawRect(rectLeft, borderTruck);
    canvas.drawRect(rectRight, borderTruck);

    var startPoint = Offset(inner.left + 1 * scale, inner.top - 50 * scale);
    var controlPoint1 = Offset(inner.left + 10 * scale, inner.top - 57 * scale);
    var controlPoint2 =
        Offset(inner.right - 10 * scale, inner.top - 57 * scale);
    var endPoint = Offset(inner.right - 1 * scale, inner.top - 50 * scale);

    var path = Path();
    path.moveTo(startPoint.dx, startPoint.dy);
    path.cubicTo(controlPoint1.dx, controlPoint1.dy, controlPoint2.dx,
        controlPoint2.dy, endPoint.dx, endPoint.dy);

    startPoint = Offset(inner.left + 3 * scale, inner.top - 40 * scale);
    controlPoint1 = Offset(inner.left + 10 * scale, inner.top - 44 * scale);
    controlPoint2 = Offset(inner.right - 10 * scale, inner.top - 44 * scale);
    endPoint = Offset(inner.right - 3 * scale, inner.top - 40 * scale);

    canvas.drawLine(Offset(inner.right, inner.top - 50 * scale),
        Offset(inner.right, inner.top - 55 * scale), borderTruck);
    canvas.drawLine(Offset(inner.left, inner.top - 50 * scale),
        Offset(inner.left, inner.top - 55 * scale), borderTruck);

    startPoint = Offset(inner.left - 3 * scale, inner.top - 54 * scale);
    controlPoint1 = Offset(inner.left + 10 * scale, inner.top - 61 * scale);
    controlPoint2 = Offset(inner.right - 10 * scale, inner.top - 61 * scale);
    endPoint = Offset(inner.right + 3 * scale, inner.top - 54 * scale);
    Path path22 = Path();
    path22.moveTo(startPoint.dx, startPoint.dy);
    path22.cubicTo(controlPoint1.dx, controlPoint1.dy, controlPoint2.dx,
        controlPoint2.dy, endPoint.dx, endPoint.dy);
    startPoint = Offset(inner.left + 3 * scale, inner.top - 40 * scale);
    controlPoint1 = Offset(inner.left + 10 * scale, inner.top - 44 * scale);
    controlPoint2 = Offset(inner.right - 10 * scale, inner.top - 44 * scale);
    endPoint = Offset(inner.right - 3 * scale, inner.top - 40 * scale);
    Path path33 = Path();
    path33.moveTo(startPoint.dx, startPoint.dy);
    path33.cubicTo(controlPoint1.dx, controlPoint1.dy, controlPoint2.dx,
        controlPoint2.dy, endPoint.dx, endPoint.dy);

    canvas.drawPath(path, borderTruck);
    canvas.drawPath(path22, borderTruck);
    canvas.drawPath(path33, borderTruck);

    Rect rect2 = Rect.fromLTWH(
      inner.left - 3 * scale,
      inner.top + 50 * scale,
      scale * 3,
      scale * 20,
    );
    Rect rect3 = Rect.fromLTWH(
      inner.left - 3 * scale,
      inner.bottom - 50 * scale,
      scale * 3,
      scale * 20,
    );

    Rect rect5 = Rect.fromLTWH(
      inner.right,
      inner.top + 50 * scale,
      scale * 3,
      scale * 20,
    );
    Rect rect6 = Rect.fromLTWH(
      inner.right,
      inner.bottom - 50 * scale,
      scale * 3,
      scale * 20,
    );

    canvas.drawRect(rect2, wheel);
    canvas.drawRect(rect3, wheel);
    canvas.drawRect(rect5, wheel);
    canvas.drawRect(rect6, wheel);

    Path path2 = new Path();

    path2.moveTo(inner.right + 5 * scale, inner.top);
    path2.lineTo(inner.right + 25 * scale, inner.top);

    path2.moveTo(inner.right + 20 * scale, inner.top);
    path2.lineTo(inner.right + 20 * scale, inner.bottom);
    canvas.drawCircle(
        Offset(inner.right + 20 * scale, inner.top), 1.5 * scale, circle);
    canvas.drawCircle(
        Offset(inner.right + 20 * scale, inner.bottom), 1.5 * scale, circle);
    path2.moveTo(inner.right + 5 * scale, inner.bottom);
    path2.lineTo(inner.right + 25 * scale, inner.bottom);

    canvas.drawPath(path2, rulerPain);
    TextUtil.drawTextWitFontSize(canvas, truck.length * 20, 12,
        inner.right + 20 * scale, inner.bottom / 2);
    Path path3 = new Path();

    path3.moveTo(inner.left, inner.bottom + 20 * scale);
    path3.lineTo(inner.right, inner.bottom + 20 * scale);

    path3.moveTo(inner.right, inner.bottom + 20 * scale);
    path3.lineTo(inner.right, inner.bottom + 2 * scale);
    canvas.drawCircle(
        Offset(inner.left, inner.bottom + 20 * scale), 1.5 * scale, circle);
    canvas.drawCircle(
        Offset(inner.right, inner.bottom + 20 * scale), 1.5 * scale, circle);
    path3.moveTo(inner.left, inner.bottom + 20 * scale);
    path3.lineTo(inner.left, inner.bottom + 2 * scale);
    TextUtil.drawTextWitFontSize(canvas, truck.width * 20, 12,
        inner.left + inner.width / 2, inner.bottom + 23 * scale);

    canvas.drawPath(path3, rulerPain);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

class TruckPainterTrail extends CustomPainter {
  TruckPainterTrail(this.truck, this.scale, this.pos);
  final Truck truck;
  final double pos;
  final double scale;
  @override
  void paint(Canvas canvas, Size size) {
    var borderTruck = Paint()
      ..strokeWidth = 2
      ..color = Colors.black
      ..style = PaintingStyle.stroke;
    var wheel = Paint()
      ..strokeWidth = 2
      ..color = Colors.black.withOpacity(0.8)
      ..style = PaintingStyle.fill;
    var ovalBox = Paint()
      ..strokeWidth = 2
      ..color = Colors.black
      ..style = PaintingStyle.stroke;
    var circle = Paint()
      ..strokeWidth = 2
      ..color = Colors.red
      ..style = PaintingStyle.fill;
    // canvas.drawRect(rect, paint);

    // double width = truck.width * scale * 2;
    // Rect inner = Rect.fromLTWH(
    //     -5 * scale, 301 * scale, width * scale + 5 * scale, 10 * scale);

    Rect inner =
        Rect.fromLTWH(this.pos * scale, 301 * scale, truck.width * scale, 10 * scale);
    Rect oval = Rect.fromLTWH(
        inner.left + 11 * scale, 303 * scale, 20 * scale, 6 * scale);
    canvas.drawRect(oval, ovalBox);
    canvas.drawCircle(Offset(inner.left + 14 * scale, 306 * scale), 5, circle);
    canvas.drawCircle(Offset(inner.left + 21 * scale, 306 * scale), 5, circle);
    canvas.drawCircle(Offset(inner.left + 28 * scale, 306 * scale), 5, circle);
    // TextUtil.drawTextInfor(canvas, "${truck.name}", 35 * scale, 303 * scale);

    Rect oval1 = Rect.fromLTWH(inner.right - 11 * scale - oval.width,
        303 * scale, 20 * scale, 6 * scale);
    canvas.drawRect(oval1, ovalBox);
    canvas.drawCircle(Offset(inner.right - 14 * scale, 306 * scale), 5, circle);
    canvas.drawCircle(Offset(inner.right - 21 * scale, 306 * scale), 5, circle);
    canvas.drawCircle(Offset(inner.right - 28 * scale, 306 * scale), 5, circle);

    canvas.drawRect(inner, borderTruck);

    Rect rect11 = Rect.fromLTWH(
      inner.left + 8 * scale,
      inner.top + 10 * scale,
      scale * 28,
      scale * 3,
    );
    Rect rect22 = Rect.fromLTWH(
      inner.right - 8 * scale - scale * 27,
      inner.top + 10 * scale,
      scale * 28,
      scale * 3,
    );
    canvas.drawRect(rect11, borderTruck);
    canvas.drawRect(rect22, borderTruck);

    Rect banh1 = Rect.fromLTWH(
      inner.left + 10 * scale,
      inner.top + 13 * scale,
      scale * 11,
      scale * 15,
    );
    Rect banh11 = Rect.fromLTWH(
      inner.left + 23 * scale,
      inner.top + 13 * scale,
      scale * 11,
      scale * 15,
    );
    Rect banh2 = Rect.fromLTWH(
      inner.right - 20 * scale,
      inner.top + 13 * scale,
      scale * 11,
      scale * 15,
    );
    Rect banh22 = Rect.fromLTWH(
      inner.right - 33 * scale,
      inner.top + 13 * scale,
      scale * 11,
      scale * 15,
    );

    canvas.drawRect(banh1, wheel);
    canvas.drawRect(banh11, wheel);
    canvas.drawRect(banh2, wheel);
    canvas.drawRect(banh22, wheel);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

class TruckPainterTrailToEdit extends CustomPainter {
  TruckPainterTrailToEdit(this.truck, this.scale, this.width, this.height);
  final Truck truck;
  final double width;
  final double scale;
  final double height;
  @override
  void paint(Canvas canvas, Size size) {
    var borderTruck = Paint()
      ..strokeWidth = 1.5
      ..color = Colors.black
      ..style = PaintingStyle.stroke;
    var ovalBox = Paint()
      ..strokeWidth = 2
      ..color = Colors.black
      ..style = PaintingStyle.stroke;
    var circle = Paint()
      ..strokeWidth = 2
      ..color = Colors.red
      ..style = PaintingStyle.fill;
    var wheel = Paint()
      ..strokeWidth = 2
      ..color = Colors.black.withOpacity(0.8)
      ..style = PaintingStyle.fill;
    var rulerPain = Paint()
      ..strokeWidth = 1
      ..color = Colors.purpleAccent
      ..style = PaintingStyle.stroke;
    var circle2 = Paint()
      ..style = PaintingStyle.fill
      ..color = Colors.purpleAccent;
    var paint = Paint()
      ..strokeWidth = 1
      ..color = Colors.black;
    var dashWidth = 5;
    var dashSpace = 5;

    Rect inner = Rect.fromLTWH(0, 401, width * scale, 10 * scale);
    double posY = inner.bottom;

    while (posY >= 180) {
      canvas.drawLine(
          Offset(inner.left, posY), Offset(inner.left, posY - dashWidth), paint);
      canvas.drawLine(
          Offset(inner.right, posY), Offset(inner.right, posY - dashWidth), paint);
      final space = (dashSpace + dashWidth);
      posY -= space;
    }

    Rect oval = Rect.fromLTWH(11 * scale, 405, 20 * scale, 6 * scale);
    canvas.drawRect(oval, ovalBox);
    canvas.drawCircle(Offset(14 * scale, 412), 4, circle);
    canvas.drawCircle(Offset(21 * scale, 412), 4, circle);
    canvas.drawCircle(Offset(28 * scale, 412), 4, circle);
    // TextUtil.drawTextInfor(canvas, "LONG VEHICLE", 32 * scale, 303 * scale);

    Rect oval1 = Rect.fromLTWH(
        inner.right - 11 * scale - oval.width, 405, 20 * scale, 6 * scale);
    canvas.drawRect(oval1, ovalBox);
    canvas.drawCircle(Offset(inner.right - 14 * scale, 412), 4, circle);
    canvas.drawCircle(Offset(inner.right - 21 * scale, 412), 4, circle);
    canvas.drawCircle(Offset(inner.right - 28 * scale, 412), 4, circle);

    canvas.drawRect(inner, borderTruck);
    TextUtil.drawTextInfor(canvas, "${truck.name}", inner.width / 2 - 40, 405);

    Rect rect11 = Rect.fromLTWH(
      inner.left + 8 * scale,
      inner.top + 10 * scale,
      scale * 28,
      scale * 3,
    );
    Rect rect22 = Rect.fromLTWH(
      inner.right - 8 * scale - scale * 27,
      inner.top + 10 * scale,
      scale * 28,
      scale * 3,
    );
    canvas.drawRect(rect11, borderTruck);
    canvas.drawRect(rect22, borderTruck);

    Rect banh1 = Rect.fromLTWH(
      inner.left + 10 * scale,
      inner.top + 13 * scale,
      scale * 11,
      scale * 15,
    );
    Rect banh11 = Rect.fromLTWH(
      inner.left + 23 * scale,
      inner.top + 13 * scale,
      scale * 11,
      scale * 15,
    );
    Rect banh2 = Rect.fromLTWH(
      inner.right - 20 * scale,
      inner.top + 13 * scale,
      scale * 11,
      scale * 15,
    );
    Rect banh22 = Rect.fromLTWH(
      inner.right - 33 * scale,
      inner.top + 13 * scale,
      scale * 11,
      scale * 15,
    );

    canvas.drawRect(banh1, wheel);
    canvas.drawRect(banh11, wheel);
    canvas.drawRect(banh2, wheel);
    canvas.drawRect(banh22, wheel);

    Path path2 = new Path();
    path2.moveTo(inner.left - 5 * scale, inner.top);
    path2.lineTo(inner.left - 15 * scale, inner.top);

    path2.moveTo(inner.left - 15 * scale, inner.top);
    path2.lineTo(inner.left - 15 * scale, inner.top - height);
    canvas.drawCircle(
        Offset(inner.left - 15 * scale, inner.top), 1.5 * scale, circle2);

    canvas.drawCircle(Offset(inner.left - 15 * scale, inner.top - height),
        1.5 * scale, circle2);
    path2.moveTo(inner.left - 5 * scale, inner.top - height);
    path2.lineTo(inner.left - 15 * scale, inner.top - height);

    canvas.drawPath(path2, rulerPain);
    TextUtil.drawTextWitFontSize(canvas, truck.height * 20, 12,
        inner.left - 15 * scale, inner.top - height / 2);

    Path path3 = new Path();

    path3.moveTo(inner.left, inner.bottom + 20 * scale);
    path3.lineTo(inner.right, inner.bottom + 20 * scale);

    path3.moveTo(inner.right, inner.bottom + 20 * scale);
    path3.lineTo(inner.right, inner.bottom + 2 * scale);
    canvas.drawCircle(
        Offset(inner.left, inner.bottom + 20 * scale), 1.5 * scale, circle2);
    canvas.drawCircle(
        Offset(inner.right, inner.bottom + 20 * scale), 1.5 * scale, circle2);
    path3.moveTo(inner.left, inner.bottom + 20 * scale);
    path3.lineTo(inner.left, inner.bottom + 2 * scale);
    TextUtil.drawTextWitFontSize(canvas, truck.width * 20, 12,
        inner.left + inner.width / 2, inner.bottom + 12 * scale);

    canvas.drawPath(path3, rulerPain);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
