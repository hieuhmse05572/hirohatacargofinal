import 'dart:math';

import 'package:flutter/material.dart';

class LineDashedPainter extends CustomPainter {
  double posX;
  double posY;
  double start;

  LineDashedPainter(this.posX, this.posY, this.start);
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..strokeWidth = 1
      ..color = Colors.black;
    var dashWidth = 5;
    var dashSpace = 5;
    while (posY >= start) {
      canvas.drawLine(
          Offset(posX, posY), Offset(posX, posY - dashWidth), paint);
      final space = (dashSpace + dashWidth);
      posY -= space;
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}

class LineDashedPainterNgang extends CustomPainter {
  double posX;
  double posY;
  LineDashedPainterNgang(this.posX, this.posY);
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..strokeWidth = 1
      ..color = Colors.black;
    canvas.drawLine(Offset(posY, posY), Offset(1000, posY), paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}

class DrawDashLine extends CustomPainter {
  Offset start;
  Offset end;
  DrawDashLine(this.start, this.end);
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..strokeWidth = 1
      ..color = Colors.red.withOpacity(0.5);
    canvas.drawLine(start, end, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}

class DrawDashLineBlack extends CustomPainter {
  Offset start;
  Offset end;
  DrawDashLineBlack(this.start, this.end);
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..strokeWidth = 2
      ..color = Colors.black;
    canvas.drawLine(start, end, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}