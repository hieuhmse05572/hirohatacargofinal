import 'package:HirohataCargo/src/models/metal.dart';
import 'package:flutter/material.dart';

import 'renderBox.dart';

Widget genWidget(Metal metal, Offset offset, double scale, bool canEdit, bool hasBothType) {
  Widget widget = Container();
  if (metal.type == 1)
    widget = Positioned(
      top: offset.dy,
      left: offset.dx,
      child: CustomPaint(
        painter: BoxPainter1(metal, scale, canEdit, hasBothType),
      ),
    );
  else if (metal.type == 2)
    widget = Positioned(
      top: offset.dy,
      left: offset.dx,
      child: CustomPaint(
        painter: BoxPainter2(metal, scale, canEdit, hasBothType),
      ),
    );
  // if (metal.type == 5 && metal.height != 0){
  //   widget = CustomPaint(
  //     painter: BoxPainterOnDrag(
  //         Rect.fromLTWH(metal.left, metal.top, metal.width, metal.height)),
  //   );}
  return widget;
}
