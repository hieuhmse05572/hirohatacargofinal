import 'package:HirohataCargo/getIt.dart';
import 'package:HirohataCargo/ui/screens/edit/draw/drawProvider.dart';
import 'package:flutter/material.dart';

class NoteCheckBox extends StatefulWidget {
  bool checked;
  int noteId;
  String note;
  NoteCheckBox({this.checked, this.noteId, this.note});

  @override
  _NoteCheckBoxState createState() => _NoteCheckBoxState();
}

class _NoteCheckBoxState extends State<NoteCheckBox> {
  final provider = getIt<DrawProvider>();

  @override
  Widget build(BuildContext context) {
    return Checkbox(
        value: widget.checked,
        onChanged: (bool value) {
          setState(() {
            widget.checked = value;
            provider.updateNoteCheckBox(widget.noteId, value, widget.note);
          });
        });
  }
}
