import 'package:HirohataCargo/getIt.dart';
import 'package:HirohataCargo/src/models/note.dart';
import 'package:HirohataCargo/src/models/truck.dart';
import 'package:HirohataCargo/src/utils/constant.dart';
import 'package:HirohataCargo/ui/screens/edit/draw/drawProvider.dart';
import 'package:HirohataCargo/ui/widgets/checkBox.dart';
import 'package:HirohataCargo/ui/widgets/loading.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'messages.dart';

// class EditTruck extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     // TODO: implement build
//     throw UnimplementedError();
//   }
// }

dialogChoosePdf(BuildContext context) {
  return Dialog(
//            contentPadding: EdgeInsets.all(5),
    elevation: 0.0,
    backgroundColor: Colors.transparent,
    child: Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      width: 420,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'PDF Chooser',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
              IconButton(
                icon: Icon(
                  Icons.clear,
                  color: Colors.purple,
                ),
                onPressed: () {
                  Navigator.pop(context, "-1");
                },
              )
            ],
          ),
          Divider(),
          SizedBox(
            height: 5,
            width: 1,
          ),
          SizedBox(
              height: 140,
              child: ListView(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context, "0");
                    },
                    child: Card(
                      child: ListTile(
                        tileColor: Colors.pink.withOpacity(0.1),
                        leading: Icon(Icons.bluetooth_sharp),
                        title: Text("Bluetooth/Wifi"),
                        // trailing: Icon(Icons.),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pop(context, "1");
                    },
                    child: Card(
                      child: ListTile(
                        tileColor: Colors.pink.withOpacity(0.1),
                        leading: Icon(Icons.save),
                        title: Text("保存"),
                        // trailing: Icon(Icons.),
                      ),
                    ),
                  ),
                ],
              ))
        ],
      ),
    ),
  );
}

Widget getStream() {
  final provider = getIt<DrawProvider>();
  return StreamBuilder<List<Note>>(
      stream: provider.streamNotes,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          List<Note> notes = snapshot.data;
          return ListView.separated(
              itemCount: notes == null ? 0 : notes.length,
              separatorBuilder: (BuildContext context, int index) => Divider(),
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  trailing: NoteCheckBox(checked: notes[index].checked, noteId: int.parse(notes[index].id), note: notes[index].note,),
                  title: Text('${notes[index].note}'),
                );
              });
        } else {
          return innerLoading();
        }
      });
}



dialogCheckNote(BuildContext context) {
  final provider = getIt<DrawProvider>();
  provider.getNotes();
  return Dialog(
    elevation: 0.0,
    backgroundColor: Colors.transparent,
    child: Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      width: 420,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                '注意書き',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
              IconButton(
                icon: Icon(
                  Icons.clear,
                  color: Colors.purple,
                ),
                onPressed: () {
                  Navigator.pop(context, "-1");
                },
              )
            ],
          ),
          Divider(),
          SizedBox(
            height: 5,
            width: 1,
          ),
          SizedBox(height: 300, child: getStream())
        ],
      ),
    ),
  );
}

dialogChangeTruck(BuildContext context) {
  return Dialog(
//            contentPadding: EdgeInsets.all(5),
    elevation: 0.0,
    backgroundColor: Colors.transparent,
    child: Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      width: 420,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                '車 種',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
              IconButton(
                icon: Icon(
                  Icons.clear,
                  color: Colors.purple,
                ),
                onPressed: () {
                  Navigator.pop(context, "0");
                },
              )
            ],
          ),
          Divider(),
          SizedBox(
            height: 5,
            width: 1,
          ),
          SizedBox(
              height: 240,
              child: ListView(
                children: [
                  _buildRow(context, Constants.truck4t),
                  _buildRow(context, Constants.truck10t),
                  _buildRow(context, Constants.truckContainer)
                ],
              ))
        ],
      ),
    ),
  );
}

Widget _buildRow(BuildContext context, Truck truck) {
  return InkWell(
    onTap: () {
      Navigator.pop(context, truck.id);
    },
    child: Card(
      child: ListTile(
        leading: Icon(Icons.directions_car),
        title: Text(truck.name),
        subtitle: Text(
            "備 考: ${truck.length * 20}, 製 品 幅: ${truck.width * 20} , 高 さ: ${truck.height * 20}"),
        // trailing: Icon(Icons.),
      ),
    ),
  );
}

class DialogUtil {
  static Future<String> showDialogChangeTruck(BuildContext context) async {
    String text = await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return dialogChangeTruck(context);
      },
    );
    return text;
  }

  static Future<String> showDialogChoosePdf(BuildContext context) async {
    String text = await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return dialogChoosePdf(context);
      },
    );
    return text;
  }

  static Future<String> showDialogNote(BuildContext context) async {
    String text = await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return dialogCheckNote(context);
      },
    );
    return text;
  }

  static Future<int> showDialogConfirm(BuildContext context, String msg) async {
    int result = await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return dialogConfirm(context, msg);
      },
    );
    return result;
  }

  static showMessages(BuildContext context, String text) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return dialogMessage(context, text);
      },
    );
  }
}
