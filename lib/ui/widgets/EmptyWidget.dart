import 'package:HirohataCargo/src/utils/constant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EmptyWiget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      color: Color(0xFFF5F5F5),
      child: Center(
        child: Column(
          children: <Widget>[
            Container(
              height: 180,
              width: 180,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/icon/icon.png"),
                  fit: BoxFit.fitWidth,
                ),
                borderRadius: BorderRadius.circular(30),
              ),
            ),
            Text(
              Messages.SEARCH_EMPTY,
              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20),
            ),
            // Text(
            //   'Không tìm thấy gì, nhập cái khác đi',
            // )
          ],
        ),
      ),
    );
  }
}
