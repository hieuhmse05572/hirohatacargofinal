import 'package:HirohataCargo/getIt.dart';
import 'package:HirohataCargo/src/utils/timer.dart';
import 'package:after_layout/after_layout.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'orderProvider.dart';

class SearchField extends StatefulWidget {
  @override
  SearchFieldState createState() {
    return new SearchFieldState();
  }
}

class SearchFieldState extends State<SearchField> with AfterLayoutMixin {
  TextEditingController _textFieldController = TextEditingController();

  final provider = getIt<OrderProvider>();
  final _timer = Debouncer(milliseconds: 200);

  bool _show = false;

  clearInput() {
    setState(() {
      _textFieldController.text = "";
    });
  }

  showIconClear() {
    setState(() {
      _show = _textFieldController.text.trim().length > 0 ? true : false;
    });
  }

  search() {
    showIconClear();
    print(_textFieldController.text);
    provider.order = _textFieldController.text.trim();
    provider.getOrders(_textFieldController.text);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: TextFormField(
        controller: _textFieldController,
        onChanged: (text) async {
          search();
        },
        onFieldSubmitted: (text) {
          search();
        },
        autocorrect: false,
        decoration: InputDecoration(
          suffix: _show
              ? InkWell(
                  onTap: () {
                    clearInput();
                    search();
                  },
                  child: Container(
                    padding: EdgeInsets.only(top: 4),
                    child: Icon(
                      Icons.cancel,
                      size: 18,
                      color: Colors.blue,
                    ),
                  ),
                )
              : null,
          prefixIcon: _getSearchIconLoading(),
          hintText: '検索',
          contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
        ),
      ),
    );
  }

  Widget _getSearchIconLoading() {
    return StreamBuilder<bool>(
        stream: provider.loadingStream,
        initialData: false,
        builder: (context, snapshot) {
          bool loading = snapshot.data;
          return loading
              ? CupertinoActivityIndicator(
                  radius: 8,
                )
              : Icon(Icons.search);
        });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    _textFieldController.text = provider.order;
    search();
  }
}
