import 'package:HirohataCargo/getIt.dart';
import 'package:HirohataCargo/src/models/glitch/NoInternetGlitch.dart';
import 'package:HirohataCargo/ui/screens/order/search_field.dart';
import 'package:HirohataCargo/ui/screens/view/view_screen2.dart';
import 'package:HirohataCargo/ui/widgets/EmptyWidget.dart';
import 'package:HirohataCargo/ui/widgets/loading.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:HirohataCargo/src/models/order.dart' as o;

import 'orderProvider.dart';

class OrderList extends StatefulWidget {
  @override
  _OrderListState createState() => _OrderListState();
}

class _OrderListState extends State<OrderList> {
  final provider = getIt<OrderProvider>();
  List<o.Order> orders = [];

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      child: _orderListWithSearch(context),
      onRefresh: () async {
        provider.getOrders(provider.order);
      },
    );
  }

  Widget _orderListWithSearch(BuildContext context) {
    return Column(
      children: <Widget>[
        SearchField(),
        Expanded(
          child: StreamBuilder(
            stream: provider.orderStream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                snapshot.data.fold((l) {
                  if (l is NoInternetGlitch) {
                    return Text("Error Timeout");
                  }
                }, (r) {
                  orders = (r);
                });

                return orders.length > 0
                    ? _orderListView(context, orders)
                    : EmptyWiget();

                // return ListView.builder(
                //   itemCount: snapshot.data.length,
                //   itemBuilder: (context, index) {
                //     return Text(snapshot.data[index]);
                //   },
                // );
              } else {
                return innerLoading();
              }
            },
            // child: orders.length == 0
            //     ? innerLoading()
            //     : _orderListView(context, orders)
            // : EmptyWiget()
          ),
        )
      ],
    );
  }

  Widget _orderListView(BuildContext context, List<o.Order> orders) {
    void _onLoading() {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return dialogLoading();
        },
      );
    }

    return new ListView.separated(
      separatorBuilder: (BuildContext context, int index) => Divider(),
      itemCount: orders.length,
      itemBuilder: (context, index) {
        // if filter is null or empty returns all data
        return ListTile(
          title: Text('${orders[index].productCode}'),
          subtitle: Text('${orders[index].scheduledShippingDate.replaceAll("-", "")}_${orders[index].tmpVehicleNumber}_${orders[index].flightCode}_${orders[index].productCode}'),
          trailing: Container(
            width: 60,
            child: Row(
              children: [
                Text('詳細'),
                Icon(
                  Icons.chevron_right,
                  color: Colors.grey,
                  size: 24.0,
                ),
              ],
            ),
          ),
          onTap: () async {
            _onLoading();
            Navigator.pop(context);
            Navigator.of(context).pushReplacement(PageRouteBuilder(
                pageBuilder: (context, animation, anotherAnimation) {
                  return ViewScreen2(order: orders[index].productCode);
                },
                transitionDuration: Duration(milliseconds: 1000),
                transitionsBuilder:
                    (context, animation, anotherAnimation, child) {
                  animation =
                      CurvedAnimation(curve: Curves.easeIn, parent: animation);
                  return Align(
                      child: FadeTransition(
                    opacity: animation,
                    child: child,
                  ));
                }));
            // Navigator.pushNamed(context, "/view");
          },
        );
      },
    );
  }

  // @override
  // void afterFirstLayout(BuildContext context) {
  //   provider.getOrders("");
  //   provider.orderStream.listen((snapshot) {
  //     snapshot.fold((l) {
  //       if (l is NoInternetGlitch) {}
  //     }, (r) {
  //       orders.addAll(r);
  //     });
  //   });
  // }
}
