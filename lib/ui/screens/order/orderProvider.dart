import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'package:HirohataCargo/src/helper/OrderHelper.dart';
import 'package:HirohataCargo/src/models/glitch/glitch.dart';
import 'package:HirohataCargo/src/models/order.dart';
import 'package:HirohataCargo/src/models/order.dart' as o;

import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';

class OrderProvider with ChangeNotifier {
  String order = "";
  final _helper = OrderHelper();
  final _streamController =
      StreamController<Either<Glitch, List<o.Order>>>.broadcast();
  final _streamControllerLoading = StreamController<bool>.broadcast();
  Stream<Either<Glitch, List<o.Order>>> get orderStream =>
      _streamController.stream;

  Stream<bool> get loadingStream => _streamControllerLoading.stream;

  refreshList(String search) {
    getOrders(search);
  }

  Future<void> getOrders(String search) async {

    _streamControllerLoading.add(true);
    final orderHelper = await _helper.getOrders(search);
    _streamController.add(orderHelper);
    _streamControllerLoading.add(false);
  }
}
