import 'dart:async';
import 'dart:core';
import 'dart:math';
import 'package:HirohataCargo/src/helper/ShapeHelper.dart';
import 'package:HirohataCargo/src/helper/TruckHelper.dart';
import 'package:HirohataCargo/src/models/glitch/glitch.dart';
import 'package:HirohataCargo/src/models/metal.dart';
import 'package:HirohataCargo/src/utils/ShapeUtil.dart';
import 'package:HirohataCargo/src/utils/constant.dart';
import 'package:HirohataCargo/src/utils/utils.dart';
import 'package:HirohataCargo/ui/painter/genWidget.dart';
import 'package:HirohataCargo/ui/painter/path.dart';
import 'package:HirohataCargo/ui/painter/renderBox.dart';
import 'package:HirohataCargo/ui/painter/renderCover.dart';
import 'package:HirohataCargo/ui/painter/renderLine.dart';
import 'package:HirohataCargo/ui/painter/renderPad.dart';
import 'package:HirohataCargo/ui/painter/renderTruck.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../src/models/truck.dart';
import '../../../src/utils/colors.dart';
import '../../../src/utils/constant.dart';
import '../../../src/utils/utils.dart';

class ViewProvider with ChangeNotifier {
  String order = '';
  bool loading = false;
  double scaleZoom = 2.72;
  double scale = 1.28;
  double lastScaleZoom = 1;
  Offset rootOffset = Offset(160.8, -208.7);
  Offset lastPoint = Offset(0, 0);
  Point bottomRightPoint = Point(0, 0);
  List<Metal> metals = [];
  List<Widget> widgets = [];
  List<Widget> nilong = [];
  double baseLine = 300;
  Metal topBox;
  Truck truck = Constants.truckContainer;
  int maxRow = 0;
  bool hasBothType;
  List<Point> lines = [];
  final _shapeHelper = ShapeHelper();
  final _truckHelper = TruckHelper();

  final _streamShapes = StreamController<List<Widget>>.broadcast();
  final _streamLoading = StreamController<bool>.broadcast();

  Stream<bool> get streamLoading => _streamLoading.stream;
  Stream<List<Widget>> get streamShapes => _streamShapes.stream;

  getMaxRowNumber() {
    if (metals != null && metals.isNotEmpty) {
      int max = metals[0].rownumber;
      metals.forEach((element) {
        if (element.rownumber > max) max = element.rownumber;
      });
      return max;
    }
    return 0;
  }

  checkHasTwoType() {
    bool type1 = false;
    bool type2 = false;
    metals.forEach((element) {
      if (element.type == 1) type1 = true;
      if (element.type == 2) type2 = true;
    });
    if (type2 && type1) return true;
    return false;
  }

  Future<void> getShapes(String order) async {
    this.order = order;
    final orderHelper = await _shapeHelper.getShapes(order);
    orderHelper.fold((l) => null, (r) {
      metals = r;
      render();
    });
  }

  reset() {
    loading = true;
  }

  Future<String> changeTypeTruck(String id) async {
    Truck temp;
    if (id == truck.id) return 'close';
    if (id == "1") {
      temp = Constants.truck4t;
    } else if (id == "2") {
      temp = Constants.truck10t;
    } else if (id == "3") {
      temp = Constants.truckContainer;
    } else
      return 'close';

    if (Util.checkTruck(metals, temp)) {
      _streamLoading.add(true);
      truck = temp;
      final result = await _truckHelper.updateTruckType(order, id);
      result.fold((l) => null, (r) {
        render();
        _streamLoading.add(false);
      });
      return "true";
    }
    return "error";
  }

  getCurrentShape(double x, double y) {
    for (var i = 0; i < metals.length; i++) {
      if (metals[i].isInner(x, y)) {
        return i;
      }
    }
    return -1;
  }

  onTapDown(Offset tapPoint) {
    getCurrentShape(tapPoint.dx - rootOffset.dx, tapPoint.dy - rootOffset.dy);
  }

  onScaleStart(Offset startPoint) {
    lastScaleZoom = scale;
    this.lastPoint = startPoint;
  }

  onScaleUpdate(ScaleUpdateDetails scaleUpdateDetails) {
    if (scaleUpdateDetails.scale != 1.0) {
      scale = (lastScaleZoom * scaleUpdateDetails.scale).clamp(0.5, 5.0);
      scaleZoom = scale * 2;
      if (scaleZoom <= 2) scaleZoom = 2;
      if (scaleZoom >= 4.5) scaleZoom = 4.5;
    }
    double dx = rootOffset.dx;
    dx += (scaleUpdateDetails.focalPoint.dx - lastPoint.dx) * scaleZoom;
    double dy = rootOffset.dy;
    dy += (scaleUpdateDetails.focalPoint.dy - lastPoint.dy) * scaleZoom;
    rootOffset = Offset(dx, dy);
    lastPoint = scaleUpdateDetails.focalPoint;
    render();
  }

  void clear() {
    loading = false;
    notifyListeners();
  }

  renderLineGreen() {

  }

  render() {
    widgets = [];
    // _streamShapes.add(widgets);
    Util.calRowNumber(metals);
    Point minMax = Util.getMinLength(metals);
    bottomRightPoint = Util.getBottomRightPoint(metals, 300);
    double withOfBloc = Util.getWidthOfBloc(metals);

    double offsetOfTruck = -(truck.width - bottomRightPoint.x) / 3;

    metals.forEach((element) {
      if (element.bottom != 300) {
        widgets.add(Positioned(
          left: rootOffset.dx,
          top: rootOffset.dy,
          child: CustomPaint(
            painter: BoxShimPainter(
                Offset(element.left * scaleZoom, element.bottom * scaleZoom),
                Offset(element.right * scaleZoom, 300 * scaleZoom)),
          ),
        ));
      }
      if (element.type != 3) {
        int rownumber = element.rownumber == null ? 0 : element.rownumber;
        double dx = rootOffset.dx;
        double dy = rootOffset.dy;
        widgets.add(Positioned(
          left: dx,
          top: dy,
          child: CustomPaint(
            painter: BoxPainterTopView(
                element,
                scaleZoom,
                minMax,
                offsetOfTruck,
                false,
                ColorsOf.colors[rownumber],
                withOfBloc + 70),
          ),
        ));
        if(element.heights > 0 || element.widths> 0 || element.lengths> 0)
        widgets.add(Positioned(
          left: dx,
          top: dy,
          child: CustomPaint(
            painter: BoxPainterTopViewBefore(
                element,
                scaleZoom,
                minMax,
                offsetOfTruck,
                false,
                ColorsOf.colors[rownumber],
                withOfBloc + 70),
          ),
        ));
      }
    });
    metals.forEach((element) {
      widgets
          .add(genWidget(element, rootOffset, scaleZoom, false, hasBothType));
    });

    topBox = Util.getTopBox(metals);
    List<Point> lines = getLine(metals);
    List<Point> listPointTopLeft = List<Point>();
    List<Point> listPointTopRight = List<Point>();
    metals.forEach((element) {
      Point pointLeft = Point(element.left, element.top);
      Point pointRight = Point(element.right, element.top);
      if (lines.contains(pointLeft)) listPointTopLeft.add(pointLeft);
      if (lines.contains(pointRight)) listPointTopRight.add(pointRight);
    });
    widgets.add(Positioned(
      left: rootOffset.dx,
      top: rootOffset.dy,
      child: CustomPaint(
        painter: LinePainter(lines, scaleZoom, baseLine),
      ),
    ));
    listPointTopRight.forEach((element) {
      if (element.x >= topBox.right) {
        widgets.add(Positioned(
          left: rootOffset.dx,
          top: rootOffset.dy,
          child: CustomPaint(painter: PadTopRightPainter(element, scaleZoom)),
        ));
      }
    });
    listPointTopLeft.forEach((element) {
      if (element.x <= topBox.left) {
        widgets.add(Positioned(
          left: rootOffset.dx,
          top: rootOffset.dy,
          child: CustomPaint(painter: PadTopLeftPainter(element, scaleZoom)),
        ));
      }
    });
    widgets.add(Positioned(
      left: rootOffset.dx,
      top: rootOffset.dy,
      child: CustomPaint(
        painter: TruckPainter(
            truck, scaleZoom, Point(withOfBloc + 70, 110), bottomRightPoint.x),
      ),
    ));
    double posX = -(truck.width - withOfBloc) / 2;

    widgets.add(Positioned(
      left: rootOffset.dx,
      top: rootOffset.dy,
      child: CustomPaint(
        painter: TruckPainterTrail(truck, scaleZoom, posX),
      ),
    ));
    metals.forEach((element) {
      if (element.pads.length > 0) {
        element.pads.forEach((pad) {
          widgets.add(Positioned(
            left: rootOffset.dx,
            top: rootOffset.dy,
            child: CustomPaint(
              painter:
                  CoverPainter(metal: element, position: pad, scale: scaleZoom),
            ),
          ));
        });
      }
    });
    renderLineGreen();
    _streamShapes.add(widgets);
  }
}
