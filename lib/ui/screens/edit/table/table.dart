import 'package:HirohataCargo/getIt.dart';
import 'package:HirohataCargo/src/models/metal.dart';
import 'package:HirohataCargo/src/models/record.dart';
import 'package:HirohataCargo/ui/screens/edit/table/recordProvider.dart';
import 'package:HirohataCargo/ui/widgets/loading.dart';
import 'package:flutter/material.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import 'builder.dart';

class TableWidget extends StatefulWidget {
  TableWidget({this.metal, this.order});
  final Metal metal;
  final String order;
  @override
  _TableWidgetState createState() => _TableWidgetState();
}

class _TableWidgetState extends State<TableWidget> {
  final provider = getIt<RecordProvider>();

  final controller = AutoScrollController(suggestedRowHeight: 20);
  animateToIndex(int index) {
    if (controller.hasClients) {
      controller.scrollToIndex(index, preferPosition: AutoScrollPosition.begin);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    provider.getRecords(widget.order);
  }

  @override
  Widget build(BuildContext context) {
    return getStream();
  }

  Widget _build(List<Record> records) {
    return Center(
        child: Column(children: <Widget>[
      Container(
//          height: 120,
        child: Column(
          children: header(11.0, records[0]),
        ),
      ),
      Expanded(
          child: Container(
              child: ListView.builder(
                  controller: controller,
                  itemCount: records.length,
                  itemBuilder: (BuildContext context, int index) {
                    return AutoScrollTag(
                        key: ValueKey(index),
                        controller: controller,
                        index: index,
                        child: buildRow(record: records[index]));
                  }))),
      bottom(
          provider.numOfIndividuals, records[0].weight, records[0].sumOfMetal)
    ]));
  }

  Widget getStream() {
    return StreamBuilder<List<Record>>(
        stream: provider.recordStream,
        builder: (context, snapshot) {
          animateToIndex(provider.scrollTo);
          if (snapshot.hasData) {
            return _build(snapshot.data);
          } else {
            return innerLoading();
          }
        });
  }

  Widget bottom(double numOfIndividuals, double totalWeight, int length) {
    double fontsize = 11;
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 30),
                child: Column(
                  children: [
                    Text("", style: TextStyle(fontSize: fontsize)),
                    Text("会計", style: TextStyle(fontSize: fontsize)),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 30),
                child: Column(
                  children: [
                    Text("員数", style: TextStyle(fontSize: fontsize)),
                    Text(
                        // "${bloc.numOfIndividuals != null ? bloc.numOfIndividuals.toInt() : bloc.numOfIndividuals}",
                        "$numOfIndividuals",
                        style: TextStyle(fontSize: fontsize)),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 30),
                child: Column(
                  children: [
                    Text("重量", style: TextStyle(fontSize: fontsize)),
                    Text(
                        // "${records[0].weight != null ? records[0].weight.toInt() : records[0].weight}",
                        "$totalWeight",
                        style: TextStyle(fontSize: fontsize)),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 30),
                child: Column(
                  children: [
                    Text("束数", style: TextStyle(fontSize: fontsize)),
                    Text(
                        // "${records.length}",
                        "$length",
                        style: TextStyle(fontSize: fontsize)),
                  ],
                ),
              )
            ],
          ),
        ],
      ),
      height: 40,
      width: double.infinity,
    );
  }

  List<Widget> header(double fontsize, Record record) => [
        Container(
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(flex: 1, child: Container()),
                  Expanded(
                      flex: 1,
                      child: Center(child: Text("* * * 積 分 明 細 指 示 書 * * *"))),
                  Expanded(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text("2 0 2 0 年 7 月 3 1 日 1 3 : 5 0"),
                      Padding(
                        padding: const EdgeInsets.only(left: 30.0, right: 30),
                        child: Text("1 頁"),
                      )
                    ],
                  )),
                ],
              )
            ],
          ),
        ),
        Row(
          children: [
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.only(left: 20.0),
                child: Text("納入先: ", style: TextStyle(fontSize: fontsize)),
              ),
            ),
            Expanded(flex: 1, child: Container()),
            Expanded(
              flex: 1,
              child: Text("本車番 : ${record.real_vehicle_number}",
                  style: TextStyle(fontSize: fontsize)),
            )
          ],
        ),
        Row(
          children: [
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.only(left: 40.0),
                child: Text("K. K給 TEL: 052-398-1758",
                    style: TextStyle(fontSize: fontsize)),
              ),
            ),
            Expanded(flex: 1, child: Container()),
            Expanded(flex: 1, child: Container()),
          ],
        ),
        Row(children: [
          Expanded(
              child: Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Text("輸送会社コード: 031", style: TextStyle(fontSize: fontsize)),
          )),
          Expanded(
              child: Text("仮車番　NO : ${record.tmp_vehicle_number}",
                  style: TextStyle(fontSize: fontsize))),
          Expanded(
              child: Text("出荷口 : 1", style: TextStyle(fontSize: fontsize))),
          Expanded(
              child: Text("出荷予定日 : ${record.scheduled_shipping_date}",
                  style: TextStyle(fontSize: fontsize))),
          Expanded(
              child: Text("便コード : ${record.flight_code}",
                  style: TextStyle(fontSize: fontsize))),
          Expanded(
              child: Text("請合コード : ${record.product_code}",
                  style: TextStyle(fontSize: fontsize))),
          Expanded(flex: 1, child: Container()),
        ]),
        Row(
          children: [
            Expanded(flex: 1, child: Container()),
            Expanded(
                flex: 1,
                child:
                    Text("<-- 結束 -->", style: TextStyle(fontSize: fontsize))),
            Expanded(flex: 2, child: Container()),
            Expanded(flex: 1, child: Container()),
          ],
        ),
        _header(11)
      ];
}

Widget _header(double fontsize) {
  return Row(
    children: [
      Expanded(
        flex: 1,
        child: Container(
            alignment: Alignment.centerRight,
            child: Text("積込順", style: TextStyle(fontSize: fontsize))),
      ),
      Expanded(
        flex: 1,
        child: Container(
            alignment: Alignment.centerRight,
            child: Text("段", style: TextStyle(fontSize: fontsize))),
      ),
      Expanded(
        flex: 1,
        child: Container(
            alignment: Alignment.centerRight,
            child: Text("幅計", style: TextStyle(fontSize: fontsize))),
      ),
      Expanded(
        flex: 1,
        child: Container(
            alignment: Alignment.centerRight,
            child: Text("例 - 例", style: TextStyle(fontSize: fontsize))),
      ),
      Expanded(
        flex: 1,
        child: Container(
            alignment: Alignment.centerRight,
            child: Text("前後", style: TextStyle(fontSize: fontsize))),
      ),
      Expanded(
        flex: 1,
        child: Container(
            alignment: Alignment.centerRight,
            child: Text("幅", style: TextStyle(fontSize: fontsize))),
      ),
      Expanded(
        flex: 1,
        child: Container(
            alignment: Alignment.centerRight,
            child: Text("高さ", style: TextStyle(fontSize: fontsize))),
      ),
      Expanded(
        flex: 1,
        child: Container(
            alignment: Alignment.centerRight,
            child: Text("長さ", style: TextStyle(fontSize: fontsize))),
      ),
      Expanded(
        flex: 2,
        child: Container(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Text("断面寸法", style: TextStyle(fontSize: fontsize)),
            )),
      ),
      Expanded(
        flex: 2,
        child: Container(
            alignment: Alignment.centerRight,
            child: Text("総員数", style: TextStyle(fontSize: fontsize))),
      ),
      Expanded(
        flex: 1,
        child: Container(
            alignment: Alignment.centerRight,
            child: Text("束数", style: TextStyle(fontSize: fontsize))),
      ),
      Expanded(
        flex: 1,
        child: Container(
            alignment: Alignment.centerRight,
            child: Text("端数", style: TextStyle(fontSize: fontsize))),
      ),
      Expanded(
        flex: 1,
        child: Container(
            alignment: Alignment.centerLeft,
            child: Text("表面", style: TextStyle(fontSize: fontsize))),
      ),
      Expanded(
        flex: 2,
        child: Container(
            alignment: Alignment.centerLeft,
            child: Text("材質", style: TextStyle(fontSize: fontsize))),
      ),
      Expanded(
        flex: 2,
        child: Container(
            alignment: Alignment.centerLeft,
            child: Text("特定品", style: TextStyle(fontSize: fontsize))),
      ),
      Expanded(
        flex: 3,
        child: Container(
            alignment: Alignment.centerLeft,
            child: Text("親類", style: TextStyle(fontSize: fontsize))),
      ),
      Expanded(
        flex: 3,
        child: Container(
            alignment: Alignment.centerLeft,
            child: Text("送状NO.", style: TextStyle(fontSize: fontsize))),
      ),
      Expanded(
        flex: 3,
        child: Container(
            alignment: Alignment.centerLeft,
            child: Text("契約NO.", style: TextStyle(fontSize: fontsize))),
      ),
    ],
  );
}
