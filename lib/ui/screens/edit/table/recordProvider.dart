import 'dart:async';
import 'dart:collection';
import 'dart:core';

import 'package:HirohataCargo/src/helper/RecordHelper.dart';
import 'package:HirohataCargo/src/models/metal.dart';
import 'package:HirohataCargo/src/models/record.dart';
import 'package:HirohataCargo/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RecordProvider with ChangeNotifier {
  String order = "";
  bool loading = true;
  List<Record> records = [];
  List<Widget> rows = [];
  List<Record> tempRecords = [];
  int scrollTo = 0;
  double numOfIndividuals = 0;

  HashMap map = HashMap<int, List<Record>>();
  final _helper = RecordHelper();

  final _streamRecord = StreamController<List<Record>>.broadcast();
  Stream<List<Record>> get recordStream => _streamRecord.stream;

  final _streamLoading = StreamController<bool>.broadcast();
  Stream<bool> get streamLoading => _streamLoading.stream;

  Future<void> getRecords(String search) async {
    tempRecords = [];
    final orderHelper = await _helper.getRecords(search);
    orderHelper.fold((l) => null, (r) {
      records = r;
      formatTable();
    });
  }

  highlightRow(Metal metal) {
    renderRow(metal);
    _streamRecord.add(tempRecords);
  }

  updateMapping(List<Metal> metals) {
    tempRecords.forEach((element) {
      metals.forEach((metal) {
        if (metal.id == element.no) {
          element.row_number = metal.rownumber.toDouble();
          element.column_number = metal.columnNumber.toDouble();
        }
      });
    });
    formatTable();
  }

  renderRow(Metal metal) {
    int count = 0;
    if (metal != null) {
      tempRecords.forEach((element) {
        if (element.ids.contains(metal.id)) {
          element.isHighlight = true;
          scrollTo = count;
        } else
          element.isHighlight = false;
        count++;
      });
    } else {
      tempRecords.forEach((element) {
        element.isHighlight = false;
      });
    }
  }

  getNumber_of_individuals(List<Record> records) {
    double sum = 0;
    records.forEach((element) {
      sum += element.number_of_individuals;
    });
    return sum;
  }

  formatTable() async {
    tempRecords = [];
    records[0].sumOfMetal = records.length;
    List<Record> t = List<Record>();
    t.add(records[0]);
    int current = 0;
    map = HashMap<int, List<Record>>();
    map[current] = t;
    for (int i = 1; i < records.length; i++) {
      if (Util.checkDuplicate(map[current][0], records[i])) {
        map[current].add(records[i]);
      } else {
        current++;
        List<Record> t = List<Record>();
        t.add(records[i]);
        map[current] = t;
      }
    }
    map.forEach((key, value) {
      Record record = map[key][0] as Record;
      record.duplicateN = map[key].length;
      List<int> ids = List();
      map[key].forEach((element) {
        ids.add(element.no);
      });
      record.ids.addAll(ids);
      record.id = key + 1;
      tempRecords.add(record);
    });

    numOfIndividuals = getNumber_of_individuals(tempRecords);
    tempRecords.forEach((element) {
      element.widthOfRow = getWidthOfRow(records, element.row_number);
    });

    Record preRecord = tempRecords[0];
    for (int i = tempRecords.length - 1; i >= 0; i--) {
      if (preRecord.row_number == tempRecords[i].row_number) {
        preRecord.row_number = 0;
      } else {}
      preRecord = tempRecords[i];
    }

    tempRecords.forEach((element) {
      element.column_range =
          "0${element.column_number.toInt()}-0${element.duplicateN.toInt() == 1 ? element.column_number.toInt() : element.column_number.toInt() + element.duplicateN.toInt() - 1}";
      if (element.row_number != 0) {
      } else
        element.widthOfRow = 0;
    });
    _streamRecord.add(tempRecords);
    // print('id');
    // tempRecords.forEach((element) {
    //   print(element.no);
    // });
  }

  getWidthOfRow(List<Record> records, double row) {
    double width = 0;
    records.forEach((element) {
      if (element.row_number == row) width += element.length;
    });
    return width;
  }

  // Widget _rowTag(double fontsize, Record record, bool isHighlight, int index,
  //     final controller) {
  //   return AutoScrollTag(
  //       key: ValueKey(index),
  //       controller: controller,
  //       index: index,
  //       child: _row(fontsize, record, isHighlight));
  // }

}
