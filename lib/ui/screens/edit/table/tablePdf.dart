import 'dart:typed_data';

import 'package:HirohataCargo/getIt.dart';
import 'package:HirohataCargo/src/models/infor.dart';
import 'package:HirohataCargo/src/models/record.dart';
import 'package:HirohataCargo/src/utils/constant.dart';
import 'package:HirohataCargo/ui/screens/edit/draw/drawProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pdf/pdf.dart';
import 'package:printing/printing.dart';
import 'package:provider/provider.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:pdf/pdf.dart';

import 'recordProvider.dart';

pw.Widget createTablePdf(List<Record> records, PdfImage pdfImage,
    PdfImage pdfImage2, pw.Font ttf, String textNote, double numOfIndividuals) {
  return pw.Stack(
      children: _genListRowPdf(
          records, pdfImage, pdfImage2, ttf, textNote, numOfIndividuals));
  // List<Record> records = bloc.records;
}

_getTable(List<Record> records, pw.Font ttf, double numOfIndividuals) {
  List<pw.Widget> list = [];
  list.addAll(header(10, ttf, records[0]));

  records.forEach((element) {
    list.add(_row(10, element, ttf));
  });
  list.add(_bottom(records[0], ttf, numOfIndividuals));
  return pw.Container(child: pw.Column(children: list));
}

_genListRowPdf(List<Record> records, PdfImage pdfImage, PdfImage pdfImage2,
    pw.Font ttf, String textNote, double numOfIndividuals) {
  List<pw.Widget> list = [];
  list.add(_getTable(records, ttf, numOfIndividuals));
  // list.addAll(header(10, ttf, records[0]));
  //
  // records.forEach((element) {
  //   list.add(_row(10, element, ttf));
  // });
  // list.add(_bottom(records[0], ttf, numOfIndividuals));
  // list.add(pw.SizedBox(height: 20, width: 1));
  // list.add(pw.Container(height: 250, width: 350, child: pw.Image(pdfImage)));

  list.add(pw.Positioned(
      top: 0,
      left: 20,
      right: 20,
      bottom: 0,
      child: pw.Row(
        mainAxisAlignment: pw.MainAxisAlignment.center,
        children: [
          pw.Expanded(
              flex: 4,
              child: pw.Container(
                child:
                    // pw.SizedBox(
                    //   height: 20,
                    //   width: 1,
                    // ),
                    pw.Text("$textNote",
                        style: pw.TextStyle(font: ttf, fontSize: 10)),
              )),
          pw.Expanded(
            flex: 4,
            child: pw.Container(),
          ),
        ],
      )));

  list.add(pw.Positioned(
      top: 100,
      left: 20,
      right: 20,
      bottom: 0,
      child: pw.Row(
        mainAxisAlignment: pw.MainAxisAlignment.center,
        children: [
          // pw.Expanded(
          //   flex: 1,
          //   child: pw.SizedBox(
          //     height: 1,
          //     width: 10,
          //   ),
          // ),
          // pw.Expanded(
          //   flex: 5,
          //   child: pw.Container(
          //       child: pw.Column(
          //     children: [
          //       // pw.SizedBox(
          //       //   height: 20,
          //       //   width: 1,
          //       // ),
          //       pw.Text("$textNote",
          //           style: pw.TextStyle(font: ttf, fontSize: 10)),
          //       pdfImage == null
          //           ? pw.Container()
          //           : pw.Image(pdfImage2, fit: pw.BoxFit.fitWidth),
          //     ],
          //   )),
          // ),
          pw.Expanded(
            flex: 4,
            child: pw.Image(pdfImage2, fit: pw.BoxFit.fitWidth),
          ),
          pw.Expanded(
            flex: 4,
            child: pw.Image(pdfImage, fit: pw.BoxFit.fitWidth),
          ),
          // pw.Expanded(
          //   flex: 1,
          //   child: pw.SizedBox(
          //     height: 1,
          //     width: 10,
          //   ),
          // ),
        ],
      )));
  return list;
}

pw.Widget _bottom(Record record, pw.Font ttf, double numOfIndividuals) {
  double fontsize = 11;
  return pw.Container(
    margin: pw.EdgeInsets.only(top: 10, bottom: 0),
    child: pw.Column(
      children: [
        pw.Row(
          mainAxisAlignment: pw.MainAxisAlignment.end,
          children: [
            pw.Padding(
              padding: const pw.EdgeInsets.only(right: 30),
              child: pw.Column(
                children: [
                  pw.Text("",
                      style: pw.TextStyle(font: ttf, fontSize: fontsize)),
                  pw.Text("会計",
                      style: pw.TextStyle(font: ttf, fontSize: fontsize)),
                ],
              ),
            ),
            pw.Padding(
              padding: const pw.EdgeInsets.only(right: 30),
              child: pw.Column(
                children: [
                  pw.Text("員数",
                      style: pw.TextStyle(font: ttf, fontSize: fontsize)),
                  pw.Text(
                      "${numOfIndividuals != null ? numOfIndividuals.toInt() : numOfIndividuals}",
                      style: pw.TextStyle(font: ttf, fontSize: fontsize)),
                ],
              ),
            ),
            pw.Padding(
              padding: const pw.EdgeInsets.only(right: 30),
              child: pw.Column(
                children: [
                  pw.Text("重量",
                      style: pw.TextStyle(font: ttf, fontSize: fontsize)),
                  pw.Text(
                      "${record.weight != null ? record.weight.toInt() : record.weight}",
                      style: pw.TextStyle(font: ttf, fontSize: fontsize)),
                ],
              ),
            ),
            pw.Padding(
              padding: const pw.EdgeInsets.only(right: 30),
              child: pw.Column(
                children: [
                  pw.Text("束数",
                      style: pw.TextStyle(font: ttf, fontSize: fontsize)),
                  pw.Text("${record.sumOfMetal}",
                      style: pw.TextStyle(font: ttf, fontSize: fontsize)),
                ],
              ),
            )
          ],
        ),
      ],
    ),
    height: 40,
    width: double.infinity,
  );
}

pw.Widget _row(double fontsize, Record record, pw.Font ttf) {
  return pw.Row(
    children: [
      pw.Expanded(
        flex: 1,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text(
                "${record.id < 10 ? '0' + record.id.toString() : record.id}",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 1,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text(
                "${record.row_number.toInt() > 0 ? (record.row_number < 10 ? '0' + record.row_number.toInt().toString() : record.row_number.toInt()) : ""}",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 1,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text(
                "${record.widthOfRow > 0 ? record.widthOfRow.toInt() : ""}",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 2,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text("${record.column_range}",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 1,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text("1",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 1,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text("${record.length.toInt()}",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 1,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text("${record.height.toInt()}",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 1,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text("${record.width.toInt()}",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 3,
        child: pw.Container(
            alignment: pw.Alignment.centerLeft,
            child: pw.Padding(
              padding: const pw.EdgeInsets.only(left: 10.0),
              child: pw.Text("${record.cross_sectional_dim}",
                  style: pw.TextStyle(font: ttf, fontSize: fontsize)),
            )),
      ),
      pw.Expanded(
        flex: 1,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text("${record.number_of_individuals}",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 1,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text("${record.duplicateN}",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 1,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text("0",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 1,
        child: pw.Container(
            alignment: pw.Alignment.centerLeft,
            child: pw.Text(
                "${record.anointingClassification == '1' ? "塗装" : "油有"}",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 2,
        child: pw.Container(
            alignment: pw.Alignment.centerLeft,
            child: pw.Text("${record.material_name}",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 2,
        child: pw.Container(
            alignment: pw.Alignment.centerLeft,
            child: pw.Text("${record.specific_product}",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 3,
        child: pw.Container(
            alignment: pw.Alignment.centerLeft,
            child: pw.Text("K5674",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 3,
        child: pw.Container(
            alignment: pw.Alignment.centerLeft,
            child: pw.Text(
                "${record.invoice_shipping_location}${record.invoice_transaction_type}${record.invoice_serial_number}${record.invoice_line_number}",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 3,
        child: pw.Container(
            alignment: pw.Alignment.centerLeft,
            child: pw.Text("${record.contract_number}",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
    ],
  );
}

buildPdf(
    context,
    List<Record> records,
    pw.Font ttf,
    Uint8List pngBytes,
    Uint8List imgDraw,
    Infor infor,
    double numOfIndividuals, bool hasBothType) {
  // Create the Pdf document
  final pw.Document doc = pw.Document();
  PdfImage image2;
  final image = PdfImage.file(
    doc.document,
    bytes: pngBytes, //Uint8List
  );
  if (imgDraw != null)
    image2 = PdfImage.file(
      doc.document,
      bytes: imgDraw, //Uint8List
    );
  else
    image2 = null;
  print('print');
  String textNote = infor.note + "\n";
  if(infor.note_checkbox_str != null)  textNote +=infor.note_checkbox_str.replaceFirst("[", "").replaceFirst("]", "") + "\n";
  if (infor.sutashon) textNote += Constants.sutansho + "\n";
  if (infor.hakka_oroshi) textNote += Constants.hakka_oroshi + "\n";
  if (infor.manbo != "") textNote += "マンボ: " + infor.manbo + "\n";
  if (infor.kobaku != "") textNote += "固縛: " + infor.kobaku + "\n";
  if (hasBothType) textNote += Constants.hasBoth + "\n";
  doc.addPage(
    pw.Page(
      pageFormat: PdfPageFormat.a4,
      margin: pw.EdgeInsets.only(left: 30, right: 10, top: 0, bottom: 20),
      orientation: pw.PageOrientation.landscape,
      build: (pw.Context context) {
        // return pw.Image(image);
        return pw.Container(
            child: createTablePdf(
                records, image, image2, ttf, textNote, numOfIndividuals));
      },
    ),
  );
  // Build and return the final Pdf file data
  return doc.save();
}

pw.Widget _header(double fontsize, pw.Font ttf) {
  return pw.Row(
    children: [
      pw.Expanded(
        flex: 1,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text(" 積込順",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 1,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text("段",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 1,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text("幅計",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 2,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text("例 ~ 例",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 1,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text("前後",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 1,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text("幅",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 1,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text("高さ",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 1,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text("長さ",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 2,
        child: pw.Container(
            alignment: pw.Alignment.centerLeft,
            child: pw.Padding(
              padding: const pw.EdgeInsets.only(left: 10),
              child: pw.Text("断面寸法",
                  style: pw.TextStyle(font: ttf, fontSize: fontsize)),
            )),
      ),
      pw.Expanded(
        flex: 2,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text("総員数",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 1,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text("束数",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 1,
        child: pw.Container(
            alignment: pw.Alignment.centerRight,
            child: pw.Text("端数",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 1,
        child: pw.Container(
            alignment: pw.Alignment.centerLeft,
            child: pw.Text("表面",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 2,
        child: pw.Container(
            alignment: pw.Alignment.centerLeft,
            child: pw.Text("材質",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 2,
        child: pw.Container(
            alignment: pw.Alignment.centerLeft,
            child: pw.Text("特定品",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 3,
        child: pw.Container(
            alignment: pw.Alignment.centerLeft,
            child: pw.Text("親類",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 3,
        child: pw.Container(
            alignment: pw.Alignment.centerLeft,
            child: pw.Text("送状NO.",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
      pw.Expanded(
        flex: 3,
        child: pw.Container(
            alignment: pw.Alignment.centerLeft,
            child: pw.Text("契約NO.",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
      ),
    ],
  );
}

List<pw.Widget> header(double fontsize, pw.Font ttf, Record record) => [
      pw.Container(
        child: pw.Column(
          children: [
            pw.SizedBox(height: 20, width: 1),
            pw.Row(
              children: [
                pw.Expanded(flex: 1, child: pw.Container()),
                pw.Expanded(
                    flex: 1,
                    child: pw.Center(
                        child: pw.Text("* * * 積 分 明 細 指 示 書 * * *",
                            style:
                                pw.TextStyle(font: ttf, fontSize: fontsize)))),
                pw.Expanded(
                    child: pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.end,
                  children: [
                    pw.Text("2 0 2 0 年 7 月 3 1 日 1 3 : 5 0",
                        style: pw.TextStyle(font: ttf, fontSize: fontsize)),
                    pw.Padding(
                      padding: const pw.EdgeInsets.only(left: 30.0, right: 30),
                      child: pw.Text("1 頁",
                          style: pw.TextStyle(font: ttf, fontSize: fontsize)),
                    )
                  ],
                )),
              ],
            )
          ],
        ),
      ),
      pw.Row(
        children: [
          pw.Expanded(
            flex: 1,
            child: pw.Padding(
              padding: const pw.EdgeInsets.only(left: 20.0),
              child: pw.Text("納入先: ",
                  style: pw.TextStyle(font: ttf, fontSize: fontsize)),
            ),
          ),
          pw.Expanded(flex: 1, child: pw.Container()),
          pw.Expanded(
            flex: 1,
            child: pw.Text("本車番 : ${record.real_vehicle_number}",
                style: pw.TextStyle(font: ttf, fontSize: fontsize)),
          )
        ],
      ),
      pw.Row(
        children: [
          pw.Expanded(
            flex: 1,
            child: pw.Padding(
              padding: const pw.EdgeInsets.only(left: 40.0),
              child: pw.Text("K. K給 TEL: 052-398-1758",
                  style: pw.TextStyle(font: ttf, fontSize: fontsize)),
            ),
          ),
          pw.Expanded(flex: 1, child: pw.Container()),
          pw.Expanded(flex: 1, child: pw.Container()),
        ],
      ),
      pw.Row(children: [
        pw.Expanded(
            flex: 2,
            child: pw.Padding(
              padding: const pw.EdgeInsets.only(left: 20.0),
              child: pw.Text("輸送会社コード: 031",
                  style: pw.TextStyle(font: ttf, fontSize: fontsize)),
            )),
        pw.Expanded(
            child: pw.Text("仮車番　NO : ${record.tmp_vehicle_number}",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
        pw.Expanded(
            child: pw.Text("出荷口 : 1",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
        pw.Expanded(
            flex: 2,
            child: pw.Text("出荷予定日 : ${record.scheduled_shipping_date}",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
        pw.Expanded(
            child: pw.Text("便コード : ${record.flight_code}",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
        pw.Expanded(
            child: pw.Text("請合コード : ${record.product_code}",
                style: pw.TextStyle(font: ttf, fontSize: fontsize))),
        pw.Expanded(flex: 1, child: pw.Container()),
      ]),
      pw.Row(
        children: [
          pw.Expanded(flex: 1, child: pw.Container()),
          pw.Expanded(
              flex: 1,
              child: pw.Text("<-- 結束 -->",
                  style: pw.TextStyle(font: ttf, fontSize: fontsize))),
          pw.Expanded(flex: 2, child: pw.Container()),
          pw.Expanded(flex: 1, child: pw.Container()),
        ],
      ),
      _header(fontsize, ttf)
    ];
