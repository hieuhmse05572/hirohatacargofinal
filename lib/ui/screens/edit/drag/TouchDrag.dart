import 'package:flutter/material.dart';

class RectsExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Rects(
          rects: [
            Rect.fromLTRB(0, 0, 40, 40),
            Rect.fromLTRB(0, 0, 80, 100),
          ],
        ),
      ),
    );
  }
}

class Rects extends StatelessWidget {
  final List<Rect> rects;
  final void Function(int) onSelected;
  final int selectedIndex;

  const Rects({
    Key key,
    @required this.rects,
    @required this.onSelected,
    this.selectedIndex = -1,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        GestureDetector(
          onTapDown: (details) {
            print("1");
          },
          child: Container(
            child: CustomPaint(
              size: Size(rects[1].width, rects[1].height),
              painter: _RectPainter(rects[1]),
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            print("2");
          },
          child: CustomPaint(
            size: Size(rects[0].width, rects[0].height),
            painter: _RectPainter(rects[0]),
          ),
        ),
      ],
    );
  }
}

class _RectPainter extends CustomPainter {
  static Paint _blue = Paint()..color = Colors.blue;

  final Rect rect;

  _RectPainter(this.rect);

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawRect(rect, _blue);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
