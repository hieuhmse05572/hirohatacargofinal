import 'dart:math';

import 'package:HirohataCargo/getIt.dart';
import 'package:HirohataCargo/src/models/metal.dart';
import 'package:HirohataCargo/ui/painter/renderBox.dart';
import 'package:HirohataCargo/ui/screens/edit/table/recordProvider.dart';
import 'package:HirohataCargo/ui/widgets/loading.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'dragProvider.dart';

class DragPad extends StatefulWidget {
  final String order;
  final bool hasBothType;
  DragPad({this.order, this.hasBothType});
  @override
  _DragPadState createState() => _DragPadState();
}

class _DragPadState extends State<DragPad> {
  final provider = getIt<DragProvider>();
  final recordProvider = getIt<RecordProvider>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    provider.getShapes(widget.order);
    provider.hasBothType = widget.hasBothType;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: Stack(
        children: [
          getStream(),
          getSelectedShapeStream(),
          _getGestureDetector(context),
          getStreamTruckArea(),
        ],
      ),
    );
  }

  Widget getStream() {
    return StreamBuilder<List<Widget>>(
        stream: provider.streamShapes,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Container(
              height: double.infinity,
              width: double.infinity,
              child: Stack(
                children: snapshot.data,
              ),
            );
          } else {
            return innerLoading();
          }
        });
  }

  Widget getStreamTruckArea() {
    return StreamBuilder<List<Widget>>(
        stream: provider.streamTruckArea,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Container(
              height: double.infinity,
              width: double.infinity,
              child: Stack(
                children: snapshot.data,
              ),
            );
          }
          return Container();
        });
  }

  Widget getSelectedShapeStream() {
    return StreamBuilder<Metal>(
        stream: provider.streamSelectedShape,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            Metal metal = snapshot.data;
            return CustomPaint(
              painter: BoxPainterOnTap(Rect.fromLTWH(
                  metal.left, metal.top, metal.width, metal.height)),
            );
          } else {
            return Container();
          }
        });
  }

  Widget _getGestureDetector(BuildContext context) {
    return GestureDetector(
        onTapDown: (details) {
          Metal metal = provider.changeCorner(details.localPosition);
          recordProvider.highlightRow(metal);
        },
        onDoubleTap: () {},
        onPanStart: (detail) {
          provider.getCurrentBox(
              detail.localPosition.dx, detail.localPosition.dy);
          // print(834.0 / 6 * 4 - detail.localPosition.dy)
        },
        onPanEnd: (detail) {
          provider.onEnd();
          recordProvider.highlightRow(null);
          // recordProvider.updateMapping(provider.metals);
        },
        onPanUpdate: (tapInfo) {
          provider.updatePositions(tapInfo.delta.dx, tapInfo.delta.dy, context);
        });
  }
}
