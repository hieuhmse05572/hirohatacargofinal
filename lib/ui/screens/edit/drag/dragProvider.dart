import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';
import 'package:HirohataCargo/getIt.dart';
import 'package:HirohataCargo/src/helper/ShapeHelper.dart';
import 'package:HirohataCargo/src/models/Level.dart';
import 'package:HirohataCargo/src/models/box.dart';
import 'package:HirohataCargo/src/models/infor.dart';
import 'package:HirohataCargo/src/models/metal.dart';
import 'package:HirohataCargo/src/models/truck.dart';
import 'package:HirohataCargo/src/services/httpConfig.dart';
import 'package:HirohataCargo/src/utils/AppConfig.dart';
import 'package:HirohataCargo/src/utils/NetworkUtil.dart';
import 'package:HirohataCargo/src/utils/constant.dart';
import 'package:HirohataCargo/src/utils/utils.dart';
import 'package:HirohataCargo/ui/painter/genWidget.dart';
import 'package:HirohataCargo/ui/painter/path.dart';
import 'package:HirohataCargo/ui/painter/renderBox.dart';
import 'package:HirohataCargo/ui/painter/renderBoxRow.dart';
import 'package:HirohataCargo/ui/painter/renderCover.dart';
import 'package:HirohataCargo/ui/painter/renderDashed.dart';
import 'package:HirohataCargo/ui/painter/renderLine.dart';
import 'package:HirohataCargo/ui/painter/renderPad.dart';
import 'package:HirohataCargo/ui/painter/renderTruck.dart';
import 'package:HirohataCargo/ui/screens/edit/draw/drawProvider.dart';
import 'package:collection/collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart';
import 'dart:ui' as ui;

import '../../../../src/utils/AppConfig.dart';

class DragProvider with ChangeNotifier {
  bool loading = true;
  List<Widget> items = [];
  List<Metal> metals = [];
  List<Metal> fronts = [];

  List<Metal> initMetals = [];
  List<Point> dashLines = [];
  List<Widget> areas = [];
  List<Widget> nilong = [];
  List<Widget> widthOfRow = [];
  String order;
  bool addVerticalWoodChanged = false;
  Truck truck;
  GlobalKey globalKey;
  bool hasBothType;

  // Infor infor;
  int selectedIndex = -1;
  Uint8List imageOfDrag = null;
  List<int> types = [];
  Point lastPoint;
  List<Metal> collisions = List();
  List<Metal> cloneCollisions = List();
  int selectedIndexByTouch = -1;
  double scale = 3;
  double lineBase = 0;
  Metal topBox;
  double base = 0;
  // double minLeft;
  Offset LeftSide;
  Offset RightSide;
  double widthOfTruck;
  double heightOfTruck;
  bool overWidth = false;
  bool underWidth = false;
  double paddingOfTruck;
  double paddingOfShape;
  bool isReset = false;
  dynamic provider;

  Queue<HashMap<int, Point>> undoBox = new Queue<HashMap<int, Point>>();
  HashMap<int, Level> mapLevelWidth = new HashMap<int, Level>();
  HashMap<int, List<Metal>> mapLevel = new HashMap<int, List<Metal>>();
  HashMap<int, double> heightMaxLevel = new HashMap<int, double>();

  final _shapeHelper = ShapeHelper();

  final _streamShapes = StreamController<List<Widget>>.broadcast();
  final _streamSelectedShape = StreamController<Metal>.broadcast();
  final _streamLoading = StreamController<bool>.broadcast();
  final _streamTruckArea = StreamController<List<Widget>>.broadcast();

  Stream<bool> get streamLoading => _streamLoading.stream;
  Stream<List<Widget>> get streamShapes => _streamShapes.stream;
  Stream<Metal> get streamSelectedShape => _streamSelectedShape.stream;
  Stream<List<Widget>> get streamTruckArea => _streamTruckArea.stream;

  int frontAndBack = 0;

  Future<void> getShapes(String order) async {
    overWidth = false;
    underWidth = false;
    isReset = false;
    this.order = order;
    final orderHelper = await _shapeHelper.getShapesToEdit(order);
    orderHelper.fold((l) => null, (r) {
      metals = r;
      init(metals);
    });
  }

  Future<void> getResetShape() async {
    overWidth = false;
    underWidth = false;
    isReset = true;
    isReset = true;
    undoBox = new Queue<HashMap<int, Point>>();
    _streamShapes.add(null);
    _streamSelectedShape.add(null);
    final orderHelper = await _shapeHelper.getResetShape(order);
    orderHelper.fold((l) => null, (r) {
      metals = r;
      init(metals);
    });
  }

  reset() {
    selectedIndexByTouch = -1;
    _streamSelectedShape.add(null);
    undoBox = new Queue<HashMap<int, Point>>();
  }

  getSelectedMetal() {
    if (selectedIndexByTouch >= 0) {
      return metals[selectedIndexByTouch];
    }
    if (selectedIndex > 0) {
      return metals[selectedIndex];
    }

    return null;
  }

  removeSelectedWidget() {
    updateWidget();
  }

  init(List<Metal> metals) {
    provider = getIt<DrawProvider>();
    initMetals = [];
    nilong = [];
    widthOfTruck = truck.width * 20 / 2000 * 300;
    heightOfTruck = truck.height * 20 / 2000 * 300;
    double minMaxOfLenth = Util.getWidthOfBloc(metals);
    paddingOfTruck = (SizeConfig.screenWidth / 13 * 6 - widthOfTruck) / 2;
    paddingOfShape = paddingOfTruck + (widthOfTruck - minMaxOfLenth) / 2;
    metals.forEach((element) {
      element.posX += paddingOfShape;
      initMetals.add(Metal.clone(element));
    });
    double min = Util.getMinOfBloc(metals);
    if (min < paddingOfTruck) overWidth = true;
    if (min > paddingOfTruck) underWidth = true;

    lineBase = Util.getLineBase(metals);
    topBox = Util.getTopBox(metals);

    renderTruck();
    updateWidget();
    updateLever(metals);
    renderLineGreen();
  }

  getInnerSide(Metal metal) {
    mapLevel.forEach((key, value) {
      List<Metal> temp = [];

      value.forEach((element) {
        if (element.type == 1) temp.add(element);
      });
      if (temp.isNotEmpty) {
        Metal min = temp[0];
        Metal max = temp[0];
        temp.forEach((element) {
          if (element.left < min.left) {
            min = element;
          }
          if (element.right > max.right) {
            max = element;
          }
        });
        nilong.add(CustomPaint(
          painter: CoverNilonPainter(metal: min, position: 4, scale: 1),
        ));
        nilong.add(CustomPaint(
          painter: CoverNilonPainter(metal: max, position: 2, scale: 1),
        ));
      }
    });

    Point topLeft = Point(metal.left + 10, metal.top - 2);
    Point topRight = Point(metal.right - 10, metal.top - 2);
    Point top = Point(metal.left + metal.width / 2, metal.top - 2);

    Point bottomLeft = Point(metal.left + 10, metal.bottom + 2);
    Point bottomRight = Point(metal.right - 10, metal.bottom + 2);
    Point bottom = Point(metal.left + metal.width / 2, metal.bottom + 2);
    int hasTop = 0;
    int hasBottom = 0;
    for (int i = 0; i < metals.length; i++) {
      if (!(metals[i].isInner(topLeft.x, topLeft.y) ||
          metals[i].isInner(topRight.x, topRight.y) ||
          metals[i].isInner(top.x, top.y))) {
        if (metals[i].id != metal.id) hasTop++;
      }
      if (!(metals[i].isInner(bottomLeft.x, bottomLeft.y) ||
          metals[i].isInner(bottomRight.x, bottomRight.y) ||
          metals[i].isInner(bottom.x, bottom.y))) {
        if (metals[i].id != metal.id) hasBottom++;
      }
      if ((metals[i].right - metal.left).abs() < 3 &&
          (metals[i].bottom - metal.bottom).abs() < 3 &&
          metals[i].type != metal.type &&
          metal.type == 1) {
        nilong.add(CustomPaint(
          painter: CoverNilonPainter(metal: metal, position: 4, scale: 1),
        ));
      }
      if ((metals[i].left - metal.right).abs() < 3 &&
          (metals[i].bottom - metal.bottom).abs() < 3 &&
          metals[i].type != metal.type &&
          metal.type == 1) {
        nilong.add(CustomPaint(
          painter: CoverNilonPainter(metal: metal, position: 2, scale: 1),
        ));
      }

      if ((metals[i].top - metal.bottom).abs() < 2 &&
          ((metals[i].left >= metal.left && metals[i].left <= metal.right) ||
              (metals[i].right >= metal.left &&
                  metals[i].right <= metal.right) ||
              (metals[i].left <= metal.left &&
                  metals[i].right >= metal.right) ||
              (metals[i].left >= metal.left &&
                  metals[i].right <= metal.right)) &&
          metals[i].type != metal.type &&
          metal.type == 1) {
        nilong.add(CustomPaint(
          painter: CoverNilonPainter(metal: metal, position: 3, scale: 1),
        ));
      }

      double start = 0;
      double end = 0;
      if (metals[i].isInner(topRight.x, topRight.y)) {
        if (metal.type == metals[i].type && metal.type == 1) {
          end = metals[i].left;
          start = metal.left;
          nilong.add(CustomPaint(
            painter: CoverNilonPainter1(
                metal: metal, position: 1, scale: 1, start: start, end: end),
          ));
          end = metals[i].right;
          start = metal.right;
          nilong.add(CustomPaint(
            painter: CoverNilonPainter1(
                metal: metals[i],
                position: 3,
                scale: 1,
                start: start,
                end: end),
          ));
        }
      } else if (metals[i].isInner(topLeft.x, topLeft.y)) {
        if (metal.type == metals[i].type && metal.type == 1) {
          end = metals[i].right;
          start = metal.right;
          nilong.add(CustomPaint(
            painter: CoverNilonPainter1(
                metal: metal, position: 1, scale: 1, start: start, end: end),
          ));

          end = metals[i].left;
          start = metal.left;
          nilong.add(CustomPaint(
            painter: CoverNilonPainter1(
                metal: metals[i],
                position: 3,
                scale: 1,
                start: start,
                end: end),
          ));
        }
      } else if (metals[i].isInner(top.x, top.y)) {
        if (metal.type == metals[i].type && metal.type == 1) {
          end = metals[i].left;
          start = metal.left;
          nilong.add(CustomPaint(
            painter: CoverNilonPainter1(
                metal: metal, position: 1, scale: 1, start: start, end: end),
          ));
          end = metals[i].right;
          start = metal.right;
          nilong.add(CustomPaint(
            painter: CoverNilonPainter1(
                metal: metal, position: 1, scale: 1, start: start, end: end),
          ));
        }
      }
      if ((metals[i].bottom - metal.top).abs() < 3 &&
          ((metals[i].left >= metal.left && metals[i].left <= metal.right) ||
              (metals[i].right >= metal.left &&
                  metals[i].right <= metal.right) ||
              (metals[i].left <= metal.left &&
                  metals[i].right >= metal.right) ||
              (metals[i].left >= metal.left &&
                  metals[i].right <= metal.right)) &&
          metals[i].type != metal.type &&
          metal.type == 1) {
        nilong.add(CustomPaint(
          painter: CoverNilonPainter(metal: metal, position: 1, scale: 1),
        ));
      }
    }
    if (hasTop == metals.length - 1 && metal.type == 1) {
      nilong.add(CustomPaint(
        painter: CoverNilonPainter(metal: metal, position: 1, scale: 1),
      ));
    }
    if (hasBottom == metals.length - 1 && metal.type == 1) {
      nilong.add(CustomPaint(
        painter: CoverNilonPainter(metal: metal, position: 3, scale: 1),
      ));
    }

    mapLevel.forEach((key, value) {
      List<Metal> temp = [];
      value.forEach((element) {
        Metal el = Metal.clone(element);
        temp.add(el);
      });
      temp.sort((a, b) {
        if (a.left > b.left)
          return 1; //1 tang dan ; -1 giam dan
        else if (a.left < b.left)
          return -1;
        else
          return 0;
      });

      for (int i = 0; i < temp.length - 1; i++) {
        if (temp[i].type == 1 && temp[i + 1].type == 1) {
          nilong.add(CustomPaint(
            painter: CoverNilonPainter1(
                metal: temp[i],
                position: 1,
                scale: 1,
                start: temp[i].right,
                end: temp[i + 1].left),
          ));
          nilong.add(CustomPaint(
            painter: CoverNilonPainter1(
                metal: temp[i],
                position: 3,
                scale: 1,
                start: temp[i].right,
                end: temp[i + 1].left),
          ));
        }
      }
    });
  }

  renderLineGreen() {
    nilong = [];
    if (hasBothType)
      for (int index = 0; index < metals.length; index++) {
        getInnerSide(metals[index]);
      }
  }
  //
  // renderLineGreen() {
  //   nilong = [];
  //   double ver = 0;
  //   double hor = 0;
  //   int type = 0;
  //   for (int index = 0; index < metals.length; index++) {
  //     Point right = Point(metals[index].right + 10, metals[index].bottom - 10);
  //     Point topLeft = Point(metals[index].left + 10, metals[index].top - 10);
  //     Point topRight = Point(metals[index].right - 10, metals[index].top - 10);
  //     Point top = Point(
  //         metals[index].left + metals[index].width / 2, metals[index].top - 10);
  //     for (var i = 0; i < metals.length; i++) {
  //       if (metals[i].isInner(right.x, right.y)) {
  //         if (metals[i].type != metals[index].type) {
  //           ver = metals[i].right;
  //           type = metals[index].type;
  //           nilong.add(CustomPaint(
  //             painter: CoverNilonPainter(
  //                 metal: metals[index], position: 2, scale: 1),
  //           ));
  //         }
  //       }
  //       double start = 0;
  //       double end = 0;
  //       if (metals[i].isInner(topRight.x, topRight.y)) {
  //         if (metals[i].type != metals[index].type) {
  //           hor = metals[i].top;
  //           type = metals[index].type;
  //
  //           end = metals[index].right + 2;
  //           start = metals[i].left;
  //           nilong.add(CustomPaint(
  //             painter: CoverNilonPainter1(
  //                 metal: metals[index],
  //                 position: 1,
  //                 scale: 1,
  //                 start: start,
  //                 end: end),
  //           ));
  //         }
  //       } else if (metals[i].isInner(topLeft.x, topLeft.y)) {
  //         if (metals[i].type != metals[index].type) {
  //           if (metals[i].top < hor) hor = metals[i].top;
  //           end = metals[index].right;
  //           start = metals[index].left;
  //           type = metals[index].type;
  //
  //           nilong.add(CustomPaint(
  //             painter: CoverNilonPainter1(
  //                 metal: metals[index],
  //                 position: 1,
  //                 scale: 1,
  //                 start: start,
  //                 end: end),
  //           ));
  //         }
  //       } else if (metals[i].isInner(top.x, top.y)) {
  //         if (metals[i].top < hor) hor = metals[i].top;
  //         type = metals[index].type;
  //
  //         nilong.add(CustomPaint(
  //           painter:
  //               CoverNilonPainter(metal: metals[index], position: 1, scale: 1),
  //         ));
  //       }
  //     }
  //   }
  //   for (int index = 0; index < metals.length; index++) {
  //     if (metals[index].top > hor && type == metals[index].type) {
  //       Point left = Point(metals[index].left - 10, metals[index].bottom - 10);
  //       Point right =
  //           Point(metals[index].right + 10, metals[index].bottom - 10);
  //       Point top = Point(metals[index].left + metals[index].width / 2,
  //           metals[index].top - 10);
  //       int has = 0;
  //       int hasRight = 0;
  //       int hasTop = 0;
  //       for (var i = 0; i < metals.length; i++) {
  //         if (!metals[i].isInner(left.x, left.y)) {
  //           has++;
  //         }
  //         if (!metals[i].isInner(right.x, right.y)) {
  //           hasRight++;
  //         }
  //         if (!metals[i].isInner(top.x, top.y)) {
  //           hasTop++;
  //         }
  //       }
  //       if (has == metals.length) {
  //         nilong.add(CustomPaint(
  //           painter:
  //               CoverNilonPainter(metal: metals[index], position: 4, scale: 1),
  //         ));
  //       }
  //       if (hasRight == metals.length) {
  //         nilong.add(CustomPaint(
  //           painter:
  //               CoverNilonPainter(metal: metals[index], position: 2, scale: 1),
  //         ));
  //       }
  //       if (hasTop == metals.length) {
  //         nilong.add(CustomPaint(
  //           painter:
  //               CoverNilonPainter(metal: metals[index], position:  1, scale: 1),
  //         ));
  //       }
  //     }
  //   }
  //
  //   updateWidget();
  // }

  getCurrentBox(double x, double y) {
    collisions = [];
    // Util.calRowNumber(metals);

    _streamSelectedShape.add(null);
    for (var i = 0; i < metals.length; i++) {
      if (metals[i].isInner(x, y)) {
        selectedIndex = i;
        lastPoint = Point(metals[i].posX, metals[i].posY);
        HashMap map = HashMap<int, Point>();
        Point point = Point(metals[i].posX, metals[i].posY);
        map[selectedIndex] = point;
        undoBox.add(map);
        updateLever(metals);
        return i;
      }
    }
    return -1;
  }

  updatePositions(double x, double y, BuildContext context) {
    selectedIndexByTouch = -1;
    dashLines = [];
    collisions = [];
    cloneCollisions = [];
    int cham = -1;
    double HEIGHT = lineBase;
    // double WIDTH = size.width / 13 * 6;
    if (selectedIndex >= 0) {
      Metal metal = metals[selectedIndex];
      double dx = x;
      double dy = y;
      metal.posX += dx;
      metal.posY += dy;
      for (var i = 0; i < metals.length; i++) {
        if ((metals[i].posX - metal.posX).abs() <= 3 &&
            metals[i].posX != metal.posX) {
          dashLines.add(Point(metals[i].posX, metals[i].posY));
        }
        if ((metals[i].right - metal.posX).abs() <= 3) {
          dashLines.add(Point(metals[i].right, metals[i].posY));
        }
        if ((metals[i].right - metal.right).abs() <= 3 &&
            metals[i].right != metal.right) {
          dashLines.add(Point(metals[i].right, metals[i].posY));
        }

        // types.add(metals[i].type);
        if (metal.checkCollision(metals[i])) {
          Metal clone = Metal.clone(metals[i]);
          // getSide(metal, clone);
          cham = (metal.getSide(clone));
          // Metal clone = Metal.clone(metals[i]);
          if (cham == 0) {
            metal.posY = clone.posY - metal.height;
          } else if (cham == 1) {
            metal.posY = clone.bottom;
          } else if (cham == 2) {
            metal.posX = clone.posX - metal.width;
          } else if (cham == 3) {
            metal.posX = clone.right;
          }
          // cloneCollisions.add(clone);
        }
      }

      // double min;
      // if (dashLines.length > 0) {
      //   min = dashLines[0].y;
      //   dashLines.forEach((element) {
      //     if (element.y < min) {
      //       min = element.y;
      //     }
      //   });
      //   dashLines.removeWhere((element) => element.y > min);
      // }

      // cloneCollisions.forEach((element) {
      //   print(cloneCollisions.length);
      //   // getSide(metal, cloneCollisions[0]);
      //   cham = (metal.getSide(element));
      //   // Metal clone = Metal.clone(metals[i]);
      //   if (cham == 0) {
      //     metal.posY = element.posY - metal.height;
      //   }
      //   if (cham == 1) {
      //     metal.posY = element.bottom;
      //   }
      //   if (cham == 2) {
      //     metal.posX = element.posX - metal.width;
      //   }
      //   if (cham == 3) {
      //     metal.posX = element.right;
      //   }
      // });

      if (metal.posX <= LeftSide.dx) metal.posX = LeftSide.dx;
      if (metal.posX >= RightSide.dx - metal.width)
        metal.posX = RightSide.dx - metal.width;
      if (metal.posY <= 0) metal.posY = 0;

      if (metal.posY >= HEIGHT - metal.height)
        metal.posY = HEIGHT - metal.height;

      // if (metal.posY >= base - metal.height) metal.posY = base - metal.height;
      updateLever(metals);
      renderLineGreen();
      updateWidget();
      dashLines.forEach((e) {
        items.add(CustomPaint(
          painter: LineDashedPainter(e.x, e.y, metal.top - 20),
        ));
      });
    }
  }

  getSide(Metal metal, Metal element) {
    double b_collision = element.bottom - metal.posY;
    double t_collision = metal.bottom - element.posY;
    double l_collision = metal.right - element.posX;
    double r_collision = element.right - metal.posX;

    if (t_collision < b_collision &&
        t_collision < l_collision &&
        t_collision < r_collision) {
      metal.posY = element.posY - metal.height;

//Top collision
    }
    if (b_collision < t_collision &&
        b_collision < l_collision &&
        b_collision < r_collision) {
//bottom collision
      metal.posY = element.bottom;
    }
    if (l_collision < r_collision &&
        l_collision < t_collision &&
        l_collision < b_collision) {
//Left collision
      metal.posX = element.posX - metal.width;
    }
    if (r_collision < l_collision &&
        r_collision < t_collision &&
        r_collision < b_collision) {
//Right collision
      metal.posX = element.right;
    }
  }

  findCurrentRow(HashMap<int, double> heightMaxLevel) {}

  onEnd() {
    if (selectedIndex == -1) return;
    Metal metal = metals[selectedIndex];
    // cloneCollisions.forEach((element) {
    //   getSide(
    //     metal,
    //     element,
    //   );
    // });
    // if (dashLines.length > 0) {
    //   dashLines.forEach((element) {
    //     if ((element.x - metal.posX).abs() <= 10 && element.x != metal.posX) {
    //       metal.posX = element.x;
    //     }
    //     if ((element.x - metal.right).abs() <= 10 && element.x != metal.right) {
    //       metal.posX = element.x - metal.width;
    //     }
    //   });
    // }
    // int currentRow = -1;
    // for (var i = 0; i < heightMaxLevel.length; i++) {
    //   if ((heightMaxLevel[i] - metal.bottom) >= 0) {
    //     currentRow++;
    //   }
    // }
    // base = heightMaxLevel[currentRow];
    // double sub = (heightMaxLevel[0] - metal.bottom).abs();
    // heightMaxLevel.forEach((key, value) {
    //   double temp = (value - metal.bottom).abs();
    //   if (temp <= sub) {
    //     sub = temp;
    //     base = value;
    //   }
    // });

    // bool col = true;
    // while (col && metal.bottom < base) {
    //   metal.posY += 0.1;
    //   if (currentRow < mapLevel.length - 1) {
    //     List<Metal> temp = [];
    //     mapLevel[currentRow + 1].forEach((element) {
    //       if (element.top > heightMaxLevel[currentRow + 1]) {
    //         temp.add(element);
    //       }
    //     });
    //     temp.forEach((element) {
    //       // if (metal.right > element.left || metal.left < element.right) {
    //       if (((element.left >= metal.left && element.left <= metal.right) ||
    //           (element.right >= metal.left && element.right <= metal.right) ||
    //           (element.left <= metal.left && element.right >= metal.right) ||
    //           (element.left >= metal.left && element.right <= metal.right))) {
    //         // if (metal.right > element.left || metal.left < element.right) {
    //         if ((metal.bottom - element.top).abs() < 10) {
    //           metal.posY = heightMaxLevel[currentRow + 1] - metal.height;
    //           col = false;
    //         }
    //       }
    //     });
    //     // print(mapLevel[currentRow].length);
    //   }
    //   if ((metal.bottom - base).abs() <= 0.2) {
    //     col = false;
    //     break;
    //   }
    //   // }
    //   if (metal.posY >= lineBase - metal.height) {
    //     break;
    //   }
    //   updateWidget();
    // }
    //
    bool col = true;
    while (col) {
      metal.posY += 0.2;
      for (var i = 0; i < metals.length; i++) {
        if (metal.checkCollision(metals[i]) &&
            metals[i].checkCollision(metal)) {
          col = false;
          break;
        }
      }
      if (metal.posY >= lineBase - metal.height) {
        break;
      }
      updateWidget();
    }

    types = [];
    // metals.sort((a, b) {
    //   if (a.left > b.left)
    //     return 1; //1 tang dan ; -1 giam dan
    //   else if (a.left < b.left)
    //     return -1;
    //   else
    //     return 0;
    // });
    // metals.forEach((element) {
    //   print('${element.rownumber} ${element.columnNumber} ${element.id}');
    // });

    updateLever(metals);
    renderLineGreen();
    updateWidget();
    selectedIndex = -1;
  }

  undo() {
    if (undoBox.length > 0) {
      _streamSelectedShape.add(null);
      HashMap map = undoBox.last;
      undoBox.removeLast();
      map.forEach((key, value) {
        metals[key].posY = value.y;
        metals[key].posX = value.x;
      });
      updateWidget();
    }
  }

  renderTruck() {
    areas = [];
    LeftSide =
        Offset((SizeConfig.screenWidth / 13 * 6 - widthOfTruck) / 2, lineBase);
    RightSide =
        Offset((SizeConfig.screenWidth / 13 * 6 - LeftSide.dx), lineBase);
    // areas.add(CustomPaint(
    //   painter: BoxWithText(text: 123, position: Offset( RightSide.dx + 20,  lineBase)),
    // ));
    // areas.add(CustomPaint(
    //   painter: LineDashedPainter(RightSide.dx, RightSide.dy),
    // ));
    areas.add(CustomPaint(
      painter: DrawDashLine(Offset(LeftSide.dx, LeftSide.dy - heightOfTruck),
          Offset(RightSide.dx, RightSide.dy - heightOfTruck)),
    ));
    areas.add(Positioned(
      left: LeftSide.dx,
      top: 0,
      child: CustomPaint(
        painter: TruckPainterTrailToEdit(
            truck, 2.3, widthOfTruck / 2.3, heightOfTruck),
      ),
    ));

    _streamTruckArea.add(areas);
  }

  findMaxHeightOfLevel(List<Metal> list) {
    double max = list[0].top;
    list.forEach((element) {
      if (element.top < max) {
        max = element.top;
      }
    });
    return max;
  }

  getWidthOfLevel(List<Metal> list) {
    double left = list[0].left;
    double right = list[0].right;
    list.forEach((element) {
      if (element.left < left) {
        left = element.left;
      }
      if (element.right > right) {
        right = element.right;
      }
    });
    return right - left;
  }

  updateLever(List<Metal> metals) {
    widthOfRow = [];
    heightMaxLevel = new HashMap<int, double>();
    mapLevel = new HashMap<int, List<Metal>>();
    List<Metal> listMetals = [];
    metals.forEach((element) {
      Metal m = Metal.clone(element);
      listMetals.add(m);
    });
    listMetals.sort((a, b) {
      if (a.bottom > b.bottom)
        return -1; //1 tang dan ; -1 giam dan
      else if (a.bottom < b.bottom)
        return 1;
      else
        return 0;
    });
    int rowNumber = 0;
    int colNumber = 1;
    double bottom = 1000;
    // Metal metal;
    // if (selectedIndex != -1) metal = listMetals[selectedIndex];
    listMetals.forEach((element) {
      if (element.type != 3) {
        if ((element.bottom.truncate() - bottom.truncate()).abs() > 2) {
          rowNumber++;
          colNumber = 0;
          mapLevel[rowNumber] = [];
          bottom = element.bottom;
        }
        if ((element.bottom.truncate() - bottom.truncate()).abs() < 2) {
          element.rownumber = rowNumber;
          colNumber++;
          element.columnNumber = colNumber;
          mapLevel[rowNumber].add(element);
        }
      }
    });
    heightMaxLevel[0] = 400;
    mapLevel.forEach((key, value) {
      heightMaxLevel[key] = findMaxHeightOfLevel(value);
      bool hasBack = false;
      double width = 0;
      for (int i = 0; i < value.length; i++) {
        if (value[i].widths != 0) {
          hasBack = true;
          width+= value[i].widths;
        }
      }
      if (hasBack)
        widthOfRow.add(CustomPaint(
          painter: BoxWithText2Line(
              text1: (getWidthOfLevel(value) / 0.15).truncateToDouble(),
              text2: ((getWidthOfLevel(value) - width) / 0.15).truncateToDouble(),
              position: Offset(RightSide.dx + 2, value[0].bottom)),
        ));
      else {
        widthOfRow.add(CustomPaint(
          painter: BoxWithText(
              text: (getWidthOfLevel(value) / 0.15).truncateToDouble(),
              position: Offset(RightSide.dx + 2, value[0].bottom)),
        ));
      }
    });
    // print(heightMaxLevel);
    // heightMaxLevel.forEach((key, value) {
    //   print(value);
    // });
  }

  updateWidget() {
    items = [];
    if (metals.isEmpty) return;
    mapLevelWidth = new HashMap<int, Level>();
    topBox = Util.getTopBox(metals);
    // render gỗ chèn dưới
    metals.forEach((element) {
      if (element.bottom != lineBase) {
        items.add(CustomPaint(
          painter: BoxShimPainter(Offset(element.left, element.bottom),
              Offset(element.right, lineBase)),
        ));
      }
    });

    // render shapes
    if (metals.isNotEmpty) {
      metals.forEach((element) {
        items.add(genWidget(element, Offset(0, 0), 1, true, hasBothType));
      });
    }

    if (provider.type == 2) {
      // items.add(Positioned(
      //   left: 0,
      //   top: 0,
      //   child: CustomPaint(
      //     painter: LinePainter(lines, 1, lineBase),
      //   ),
      // ));
      items.add(CustomPaint(
          painter: DrawText(
        text: "ゴム",
        position: Offset(LeftSide.dx + 160, topBox.top - 10),
      )));

      items.add(CustomPaint(
        painter: DrawDashLineBlack(Offset(LeftSide.dx + 130, topBox.top - 50),
            Offset(LeftSide.dx + 30, topBox.top - 5)),
      ));
      items.add(CustomPaint(
        painter: DrawDashLineBlack(Offset(RightSide.dx - 130, topBox.top - 50),
            Offset(RightSide.dx - 30, topBox.top - 5)),
      ));
    }
    metals.forEach((element) {
      if (element.pads.length > 0) {
        element.pads.forEach((pad) {
          items.add(CustomPaint(
            painter: CoverPainter(metal: element, position: pad, scale: 1),
          ));
        });
      }
    });
    items.addAll(nilong);
    items.addAll(widthOfRow);

    if (provider.type == 1) {
      items.add(CustomPaint(
          painter: DrawText(
        text: "カド当て",
        position: Offset(LeftSide.dx + 150, topBox.top - 10),
      )));

      // render dây chằng
      List<Point> lines = getTopLine(metals);
      if (lines.length > 2) {
        List<Point> leftPoint = [];
        List<Point> rightPoint = [];
        List<Point> listPoint = [];
        lines.forEach((element) {
          if (element.y.round() < topBox.bottom.round() - 2) {
            listPoint.add(element);
          }
        });
        listPoint.sort((a, b) {
          if (a.x > b.x)
            return 1; //1 tang dan ; -1 giam dan
          else if (a.x < b.x)
            return -1;
          else
            return 0;
        });

        leftPoint.add(listPoint.first);
        listPoint.forEach((element) {
          if (element.y.round() < leftPoint.last.y.round() &&
              element.x.round() > leftPoint.last.x.round()) {
            leftPoint.add(element);
          }
        });
        leftPoint.forEach((element) {
          items.add(Positioned(
            left: 0,
            top: 0,
            child: CustomPaint(painter: PadTopLeftPainter(element, 1)),
          ));
        });
        // print('----');
        // leftPoint.forEach((element) {
        //   print(element);
        // });
        listPoint = listPoint.reversed.toList();
        rightPoint.add(listPoint.first);
        listPoint.forEach((element) {
          if (element.y.round() < rightPoint.last.y.round() &&
              element.x.round() < rightPoint.last.x.round() &&
              element.x > leftPoint.last.x) {
            rightPoint.add(element);
          }
        });
        rightPoint.forEach((element) {
          items.add(Positioned(
            left: 0,
            top: 0,
            child: CustomPaint(painter: PadTopRightPainter(element, 1)),
          ));
        });
      }
    }
    _streamShapes.add(items);
  }

  changeCorner(Offset touch) {
    // Util.calRowNumber(metals);

    int index = -1;
    for (var i = 0; i < metals.length; i++) {
      if (metals[i].isInner(touch.dx, touch.dy)) {
        index = i;
        break;
      }
    }
    if (index != -1) {
      selectedIndexByTouch = index;
      Metal metal = metals[index];
      _streamSelectedShape.add(metal);

      return metal;
    } else {
      selectedIndexByTouch = -1;
      _streamSelectedShape.add(null);
      return null;
    }
  }

  addVeritcalWood(int position) {
    addVerticalWoodChanged = true;
    Metal metal = metals[selectedIndexByTouch];
    if (metal.pads.contains(position)) {
      metal.pads.remove(position);
    } else {
      metal.pads.add(position);
    }
    updateWidget();
  }

  checkVerticalWoodChanged() {
    String initStr = "";
    String str = "";
    for (var i = 0; i < initMetals.length; i++) {
      initStr += "$i:";
      str += "$i:";
      if (initMetals[i].pads.length > 0)
        initMetals[i].pads.sort((a, b) {
          if (a > b)
            return -1; //1 tang dan ; -1 giam dan
          else if (a < b)
            return 1;
          else
            return 0;
        });
      if (metals[i].pads.length > 0)
        metals[i].pads.sort((a, b) {
          if (a > b)
            return -1; //1 tang dan ; -1 giam dan
          else if (a < b)
            return 1;
          else
            return 0;
        });
      initMetals[i].pads.forEach((element) {
        initStr += element.toString();
      });
      metals[i].pads.forEach((element) {
        str += element.toString();
      });
      initStr += "+";
      str += "+";
    }
    if (initStr == str) return false;
    return true;
  }

  getPositionInnerRect(Offset offset, Metal metal) {
    double x = offset.dx;
    double y = offset.dy;
    if ((metal.right - x).abs() <= 20) return 2;
    if ((metal.left - x).abs() <= 20) {
      return 4;
    }
    return -1;
  }

  canSave() {
    double minOfBloc = Util.getMinOfBloc(metals);
    double maxOfBloc = Util.getWidthOfBloc(metals);
    // double padding =
    //     (((paddingOfShape - paddingOfTruck)).roundToDouble()).abs();
    if (minOfBloc < LeftSide.dx) return true;
    if (maxOfBloc > RightSide.dx) return true;
    if (topBox.top < LeftSide.dy - heightOfTruck) return true;
    return false;
  }

  Future<String> save() async {
    selectedIndexByTouch = -1;
    _streamSelectedShape.add(null);
    String body = "";
    double padding = 0;
    if (!isReset && overWidth) {
      padding = (((paddingOfShape - paddingOfTruck)).roundToDouble()).abs();
    }
    if (!isReset && underWidth) {
      padding =
          (((paddingOfShape - paddingOfTruck)).roundToDouble()).abs() * -1;
    }
    metals.forEach((element) {
      Metal revert =
          Metal.revert(element, Configs.scaleToEdit, paddingOfShape + padding);
      body += revert.toJson() + ',';
    });
    body = Util.removeLastCharacter(body);
    body = "[ " + body + " ]";
    try {
      _streamShapes.add(null);
      Response res = await post(NetworkUtil.BASE_URL + '/api/update',
              headers: NetworkUtil.getRequestHeaders(), body: body)
          .timeout(
        const Duration(seconds: 5),
      );
      if (res.statusCode == 200) {
        // undoBox = new Queue<HashMap<int, Point>>();
        addVerticalWoodChanged = false;
        isReset = false;
        _streamShapes.add(items);
        return Network.SUCCESS;
      }
    } on TimeoutException catch (_) {
      return Network.TIMEOUT;
    }
    _changeLoadingState(false);
    return Network.ERROR;
  }

  _changeLoadingState(bool state) {
    loading = state;
    notifyListeners();
  }

  Future<Uint8List> capturePng() async {
    try {
      RenderRepaintBoundary boundary =
          globalKey.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage(pixelRatio: 3.0);
      ByteData byteData =
          await image.toByteData(format: ui.ImageByteFormat.png);
      imageOfDrag = byteData.buffer.asUint8List();
    } catch (e) {
      print(e);
    }
    return null;
  }
}
