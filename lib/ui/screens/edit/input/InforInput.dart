import 'package:HirohataCargo/getIt.dart';
import 'package:HirohataCargo/src/models/infor.dart';
import 'package:HirohataCargo/ui/screens/edit/draw/drawProvider.dart';
import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class InforInput extends StatefulWidget {
  final Infor infor;
  InforInput({this.infor});
  @override
  _CustomInputState createState() => _CustomInputState();
}

class _CustomInputState extends State<InforInput> with AfterLayoutMixin {
  final provider = getIt<DrawProvider>();

  TextEditingController textEditingController1 = TextEditingController();
  TextEditingController textEditingController2 = TextEditingController();
  bool topVal = false;
  bool botVal = false;

  @override
  Widget build(BuildContext context) {
    // final dataModel = Provider.of<DrawPadBloc>(context, listen: false);
    // if(provider.initLoading) {

    // initLoading = false;
    // }
    return Container(
      margin: EdgeInsets.only(top: 20, bottom: 30, left: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                  width: 50,
                  child: Text("マンボ ")),
              Container(
                width: 80,
                height: 30,
                child: TextFormField(
                  maxLength: 4,
                  controller: textEditingController1,
                  inputFormatters: <TextInputFormatter>[
                    WhitelistingTextInputFormatter.digitsOnly
                  ],
                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                  // initialValue:
                  // dataModel.infor == null ? "" : dataModel.infor.manbo,
                  onChanged: (text) {
                    provider.updateInput(1, "$text");
                    // textEditingController1.text = text;
                  },
                  decoration: new InputDecoration(
                    counterText: "",
                    suffixText: " 本",
                    contentPadding: new EdgeInsets.symmetric(
                        vertical: 0.0, horizontal: 10.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Colors.greenAccent, width: 1.0),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 1.0),
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 3,
            width: 1,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                  width: 50,
                  child: Text("固縛 ")),
              Container(
                width: 80,
                height: 30,
                child: TextFormField(
                  maxLength: 4,
                  controller: textEditingController2,
                  inputFormatters: <TextInputFormatter>[
                    WhitelistingTextInputFormatter.digitsOnly
                  ],
                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                  onChanged: (text) {
                    provider.updateInput(2, "$text");
                    // textEditingController2.text = text;
                  },
                  decoration: new InputDecoration(
                    counterText: "",
                    suffixText: " 点",
                    contentPadding: new EdgeInsets.symmetric(
                        vertical: 0.0, horizontal: 10.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Colors.greenAccent, width: 1.0),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 1.0),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  void afterFirstLayout(BuildContext context) {
    textEditingController1.text =
        widget.infor == null ? "" : widget.infor.manbo;
    textEditingController2.text =
        widget.infor == null ? "" : widget.infor.kobaku;
    // TODO: implement afterFirstLayout
  }
}
