import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:HirohataCargo/src/helper/AdditionInforHelper.dart';
import 'package:HirohataCargo/src/helper/NoteHelper.dart';
import 'package:HirohataCargo/src/models/infor.dart';
import 'package:HirohataCargo/src/models/note.dart';
import 'package:HirohataCargo/src/utils/AppConfig.dart';
import 'package:HirohataCargo/src/utils/NetworkUtil.dart';
import 'package:HirohataCargo/src/utils/constant.dart';
import 'package:HirohataCargo/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart';
import 'dart:ui' as ui;

class DrawProvider with ChangeNotifier {
  // Infor infor;
  GlobalKey globalKey;
  bool initLoading = true;
  Infor infor = Infor("", "", "", false, "", "", false, "", 0);
  int type = 0;
  // Infor("A1", "", Constants.drawing, true, "manbo", "kobaku", false);
  bool loading = true;
  bool hadClear = false;
  Uint8List imageOfDraw = null;
  List<Offset> points = <Offset>[];
  List<Offset> temp = <Offset>[];
  bool hadSave = false;
  List<int> toUndo = List();
  int startPoint;
  Infor initInfor;
  final _streamInfor = StreamController<Infor>.broadcast();
  Stream<Infor> get streamInfor => _streamInfor.stream;
  final _streamPoints = StreamController<List<Offset>>.broadcast();
  Stream<List<Offset>> get streamPoints => _streamPoints.stream;

  String noteCheckBox = "";

  List<int> selectedNotes = [];
  List<String> selectedNotesStr = [];

  final _streamNotes = StreamController<List<Note>>.broadcast();
  Stream<List<Note>> get streamNotes => _streamNotes.stream;

  final _additionInforHelper = AdditionInforHelper();
  final _noteHelper = NoteHelper();

  clear() {
    hadClear = true;
    points = <Offset>[];
    toUndo = <int>[];
    // _streamPoints.add(points);
  }

  Future<void> getInfor(String order) async {
    selectedNotes = [];
    final inforHelperResult =
        await _additionInforHelper.getAdditionInfor(order);
    inforHelperResult.fold((l) => null, (r) {
      infor = r;
      selectedNotes.addAll(stringToList(infor.note_checkbox));
      initInfor = Infor.clone(infor);
      _streamInfor.add(infor);
      points.addAll(getListList());
      _streamPoints.add(points);
    });
  }

  Future<List<Note>> getNotes() async {
    selectedNotesStr = [];
    final noteHelper = await _noteHelper.getNotes();
    noteHelper.fold((l) => null, (r) {
      r.forEach((element) {
        if (selectedNotes.contains(int.parse(element.id))) {
          element.checked = true;
          selectedNotesStr.add(element.note);
        }
      });
      _streamNotes.add(r);
      infor.note_checkbox_str = selectedNotesStr.toString();
      _streamInfor.add(infor);
    });
  }

  List<int> stringToList(String str) {
    if (str == null || str == '') return [];
    var split = str.split(",");
    List<int> list = [];
    split.forEach((element) {
      int e = int.parse(element);
      list.add(e);
    });
    return list;
  }

  String listToString(List<int> list) {
    String str = "";
    list.forEach((element) {
      str += element.toString();
    });
    return str;
  }

  Future<Uint8List> capturePng() async {
    try {
      RenderRepaintBoundary boundary =
          globalKey.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage(pixelRatio: 3.0);

      ByteData byteData =
          await image.toByteData(format: ui.ImageByteFormat.png);
      imageOfDraw = byteData.buffer.asUint8List();
    } catch (e) {
      print(e);
    }
    return null;
  }

  onPanStart(DragStartDetails details) {
    points = new List.from(points)..add(details.localPosition);
  }

  onTapDown(TapDownDetails details) {
    points = new List.from(points)..add(details.localPosition);
  }

  onPanDown(DragDownDetails details) {
    points = new List.from(points)..add(details.localPosition);
  }

  onPanUpdate(Offset details) {
    if (details.dx < 0 || details.dx > SizeConfig.screenWidth / 13 * 5) return;
    if (details.dy < 165) return;
    // Offset _localPosition = object.globalToLocal(details.globalPosition);
    points = new List.from(points)..add(details);
    _streamPoints.add(points);
  }

  onPanEnd() {
    startPoint = points.length - startPoint;
    toUndo.add(startPoint);
    points.add(null);
  }

  toJson() {
    String json = "";
    points.forEach((element) {
      if (element != null) {
        double dx = element.dx;
        double dy = element.dy;
        json += "\{ 'dx':$dx, 'dy': $dy \} ,";
      } else {
        json += "\{ 'dx':-1.0, 'dy': -1.0 \} ,";
      }
    });
    json = Util.removeLastCharacter(json);
    String drawing = "[ " + json + "]";
    infor.drawing = drawing;
  }

  getListList() {
    List<Offset> points = <Offset>[];
    if (infor.drawing != "") {
      infor.drawing = infor.drawing.replaceAll("\'", "\"");
      final data = jsonDecode(infor.drawing);
      if (data != null && data.length > 0) {
        for (Map i in data) {
          // print(i);
          Offset point = Offset(i["dx"], i["dy"]);
          if (point.dx == -1.0 && point.dy == -1.0) {
            points.add(null);
          } else
            points.add(point);
        }
      }
    }
    return points;
  }

  undo() {
    if (toUndo.length > 0) {
      temp = <Offset>[];
      temp.addAll(points);
      int length = temp.length;
      int rangeUndo = toUndo.last;
      temp.removeRange(length - rangeUndo - 1, length);
      toUndo.removeLast();
      points = <Offset>[];
      points.addAll(temp);
      // _streamPoints.add(points);
    }
  }

  reset() {
    points = <Offset>[];
    imageOfDraw = null;
    toUndo = List();
    hadClear = false;
    hadSave = false;
  }

  init() {
    infor = Infor(infor.productCode, "", "", false, "", "", false, "", 0);
  }

  updateType(int type) {
    infor.mode = type;
    this.type = type;
  }

  updateNoteCheckBox(int note_id, bool value, String note) {
    if (value) {
      if (!selectedNotes.contains(note_id)) selectedNotes.add(note_id);
      selectedNotesStr.add(note);
      infor.note_checkbox_str = selectedNotesStr.toString();
      _streamInfor.add(infor);
    } else {
      selectedNotes.remove(note_id);
      selectedNotesStr.remove(note);
      infor.note_checkbox_str = selectedNotesStr.toString();
      _streamInfor.add(infor);
    }
  }

  updateCheckBox(int checkbox, bool value) {
    if (checkbox == 1)
      infor.sutashon = value;
    else
      infor.hakka_oroshi = value;
    _streamInfor.add(infor);
  }

  updateInput(int text, String content) {
    if (text == 1)
      infor.manbo = content;
    else
      infor.kobaku = content;
    _streamInfor.add(infor);
  }

  checkChangedInfo() {
    // toJson();
    if (infor.compareTo(initInfor)) return false;
    return true;
  }

  Future<String> save() async {
    String body = "";
    toJson();
    infor.note_checkbox =
        selectedNotes.toString().replaceFirst("[", "").replaceFirst("]", "");
    body = infor.toJson();

    try {
      // _changeLoadingState(true);

      Response res = await post(NetworkUtil.BASE_URL + '/api/updateInfo',
              headers: NetworkUtil.getRequestHeaders(), body: body)
          .timeout(
        const Duration(seconds: 10),
      );
      if (res.statusCode == 200) {
        initInfor = Infor.clone(infor);
        // toUndo = List();
        // _streamPoints.add(points);
        hadSave = true;
        return Network.SUCCESS;
      }
    } on TimeoutException catch (_) {
      return Network.TIMEOUT;
    } on Exception catch (e) {
      print(e);
    }
    _changeLoadingState(false);
    return Network.ERROR;
  }

  _changeLoadingState(bool state) {
    loading = state;
    notifyListeners();
  }
}
